# Drutopia Related Content

This module enables Drutopia's related content feature.

At the bottom of articles, blogs, etc. will be shown related articles, blogs, etc.  These relations are based on taxonomy terms.
