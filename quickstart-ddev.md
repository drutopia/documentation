# Quickstart with DDEV

We will end with the Drutopia distribution codebase, built by Composer, in a directory called `my-drutopia-site` (please change this to whatever you would like in the instructions below!).


## Prerequisite

1. [Install DDEV](https://ddev.com/get-started/)


## Instructions

```{warning}
This will clone items from GitLab; until we change this please [add your SSH key to GitLab.com](https://docs.gitlab.com/ee/user/ssh.html).
```

1. Make a directory for your new site project and move into it, for example `mkdir my-drutopia-site` and `cd my-drutopia-site`.

2. Run these commands in a terminal to set up DDEV and install the code base using Composer.

    ```bash
    ddev config --docroot=web --project-type=drupal10 --webserver-type=apache-fpm --database=mariadb:10.8 --php-version=8.2 --create-docroot
    ddev auth ssh
    ddev composer create drutopia/drutopia_template:2.x-dev --no-interaction
    ```
    When prompted (it will be once for each), type your sudo password and press enter for Yes.

3. Back on your host machine, you can use `ddev drush status` to check that Drupal is ready and to get the URL for the site.  Visit the URL and install Drutopia in the language and with the sub-profiles you choose.


```{note}
Optionally, employ [pinning](pinning) to ensure only versions of software tested with a release of Drutopia is used.
```
