# Drutopia technical guide

## Decision Making

If a technical issue arises that is not covered in tech guide then the developer is empowered to make a decision on their own but then have their work reviewed by a tech lead.

Tech Leads are:
* Ben
* Clayton
* Nedjo
* Rosemary

## Principles and process

### Principles

* Drutopia features should provide the basic functionality that most sites or distributions will need.
* Any advanced or specialized functionality should be provided in separate features that require more basic features.
* The ease of discovery and use of website feature for both admins and end users should be an overriding goal.
* Every main task should have only one primary admin interface.
* Common protocols and solutions should be used to ensure consistency and ease of use.

### Uniqueness and compatibility

Wherever possible, each Drutopia feature should cover a unique area of functionality. For example, there should be only a single Drutopia feature
that provides general blog functionality. Exceptions can be made where the underlying framework used is radically different and there is no common
base to build on.

## Available on the Drutopia infrastructure, mirrored on drupal.org

To ensure visibility, all Drutopia features should be hosted on the Drutopia infrastructure (currently on GitLab) and mirrored on drupal.org.

### Standard solutions

To help produce uniformity among the various Drutopia features, standard solutions are adopted across Drutopia. For example, for mapping, there may be several Drupal modules available,--Leaflet, OpenLayers, and so on. To ensure consistency, one should be designated as the Drutopia standard.

In selecting between alternatives, the [Drupal principles](http://drupal.org/principles) are relevant as well as the following additional criteria:

* _Community free software_: The candidate solution is a community-driven free software solution.
* _Non-proprietary_: The candidate solution is not limited to, branded by, or controlled by a single company.
* _Secure_: Any module must have a stable release on Drupal.org and so be supported by the Drupal security team.
* _Data security_: The candidate solution does not require posting data to an external source unless doing so is the explicit purpose.
* _Works out of the box_ without requiring manual configuration, e.g., entry of an API key.  (Except where required, as for payment processing)

Before introducing a new Drutopia feature that introduces a dependency that could be considered as a new standard solution, an issue must be posted on the Drutopia project proposing the new standard solution with justification.

Adopted standard solutions:

* [Pathauto](https://www.drupal.org/project/pathauto): where appropriate, paths variables should be set so that, if Pathauto is present, content types defined by a feature will get appropriate
paths.
* [Rules](https://www.drupal.org/project/rules) should be used for any configurable actions.

### Opportunity for peer review

There should be an opportunity for review prior to a new Drutopia feature being posted. Such review will help ensure that new features indeed meet the Drutopia spec, e.g.,

* identify any potential components that could/should be pulled into more specific Drutopia features,
* identify properties that are too site-specific to warrant inclusion in Drutopia,
* if the feature introduces a new solution to standardize (e.g., an external library or a way to structure particular data), ensure a consensus prior to adoption.

Therefore each proposed Drutopia feature should be posted first as an issue on the drutopia-distribution project, with a link to the sandbox version of the proposed feature and two weeks allowed for peer review.

## Technical guidelines

### Configuration Altering

Sometimes we need to alter the configuration provided by a feature. We do this by using [Config Actions](https://www.drupal.org/project/config_actions).


### Naming and namespaces

#### Code namespace

A Drutopia feature must include the Drutopia namespace.

* Example: `drutopia_event`.

In the Features module, this namespace is provided by a features _bundle_. A feature is named for the combination of a bundle prefix (`drutopia`) and a short name (`event`).

#### Item naming
Individual configuration items should be named using the short name of the target feature where possible. Examples:

* Machine name of a content type: `event`.
* Machine name of a field storage specific to blogs: `field_event_date`.

### User roles and permissions

#### User roles
Drutopia features should use the following roles as appropriate.

* _administrator_ is used for site administration tasks, such as installing and configuring modules, content types, and blocks.
* _contributor_ is a Drupal user who contributes content, e.g., a staff member at an organization or company.
* _editor_ is a Drupal user responsible for editing and administrating content, taxonomies, and comments.

These roles will be provided by the drutopia_core feature and so will be present if Drutopia is installed.

A Drutopia feature may provide additional roles, but such roles should have a scope no greater than the scope of the feature. For example, a blog feature might provide a _blogger_ role. However, a technology blog feature should not provide a _blogger_ role, since that role would be relevant to a more general blog feature.

#### Permissions

The editor role should not be expected to have _administer nodes_ permissions.

For each content type introduced, roles should be assigned as follows:

* _contributor_: _create [type] content_, _delete own [type] content_, _edit own [type] content_
* _editor_: _create [type] content_, _delete all [type] content_, _edit all [type] content_

where [type] is the machine name of the content type being introduced by the feature.

Note that user permissions are not yet supported in the Features module--see [this issue](https://www.drupal.org/node/2383439).

### Paths and breadcrumbs

#### Primary path and nesting

Where a feature provides content or nested pages, these should wherever feasible be available at paths nested below the feature's primary path.

A feature that defines a content type should register a pathauto pattern that nests content of that type below the feature's primary page.

Example: a blog feature would set a pathauto pattern to `blog/[node:title]`.

The machine name of the pattern should be in the form `[entity type]_[bundle]`. Example: `node_blog`.

#### Menu location and breadcrumbs

Menu location and breadcrumbs for pages created by the feature should match the nesting used for paths.

### Block visibility and theme regions

#### Block visibility

TBD: what page layout solution are we adopting?

### Theme regions

TBD: how are we approaching theme regions?

### Code and dependencies

A feature may not depend on any feature that is not fully compliant with this specification.

#### Versions

Features should use the recommended stable release versions of contributed modules and themes.

#### Libraries

Any external libraries used by a feature should be installed in a libraries directory if possible.

#### Patches

Patches should be avoided wherever possible. Ideally, patches should be used only up to and including beta releases, to include functionality expected to reach stable releases by the time the feature is out of beta.

Where feasible, patches should be replaced with workarounds in a feature's custom code that use Drupal APIs to achieve the same ends. These workarounds should include documentation identifying the patch that they relate to so they can be removed if/when that patch is accepted.

#### Drush make file/Composer

TBD: how should code dependencies be handled?

### Content types and fields

Where a feature defines a content type, the following guidelines should be followed:

#### Image fields

* If a single image is desired to represent each piece of content in the content type, the content type should use field_image as used in e.g.
drutopia_article.
* If media (images, audio, video, files) are desired, the content type should use field_media as used in e.g. drutopia_article.

### Views

For consistency, the following conventions should be used for views included in features.

* Naming: if the view is primarily for presenting a single content type, it should be named for the machine name of that content type.
* At a minimum, views of a content type should include a page, block, and feed display, with the feed display attached to both the page and block. The
page display should have a path matching the content type's machine name and the feed should have the same path with an .xml extension.

### Help

A feature may include contextual help provided through the regular Drupal help API covering major components of the feature.
