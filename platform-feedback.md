Feedback Workflow
-----------------

Feedback is welcomed and encouraged from platform members. The workflow for feedback is as follows -

1. Software member reports bug, requests feature or asks question in their GitLab project.
2. Drutopia team member assigns issue to themself
3. Drutopia team member determines if the issue is member-specific or distribution-worthy  
4a. If specific to the member, issue is handled depending on capacity and platform-compatibility  
4b. If distribution-worthy, corresponding issue is created in the appropriate Drutopia project, with a link back to the original member-reported issue.
5. Distribution-worthy issue is evaluated based on the `Drutopia criteria <http://docs.drutopia.org/en/latest/creating-a-distribution.html?highlight=resourced#criteria>`_.
6. Outcome of issue is shared with reporting member in their original issue.

