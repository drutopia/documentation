# Hosting with Ansible

All components of Drutopia hosting are public as free/libre open source software as part of our commitment to [LibreSaaS](https://libresaas.org/).

But you don't need to know about any of this to have a site on the Drutopia platform.

Nor do you need to use this approach to hosting your own Drutopia site(s), but you can!

As befitting techincal documentation, the main resource is the [Drutopia Host README](https://gitlab.com/drutopia-platform/drutopia_host/blob/master/README.md)

Every site on the platform has the concept of a *custom repository*—which is used only to get configuration, templates, and other non-PHP theme files like CSS and JavaScript—and a *drutopia_version* which is where all the code is defined.

For a truly custom site, as part of Drutopia's *platform as a service* or on your own server, you could have your site's repository available as a `drutopia_version` within the host definition.

Here is the documentation for Drutopia's private repository.  This is for example only (or Drutopia maintainers) as the private hosting list of sites is indeed private.

## Setup

In order to run builds for the official server(s):

- If not already, clone `drutopia_host`
- Clone this project into a subfolder of `drutopia_host`
- Also clone the `build_artifacts` site under the `drutopia_host` project

The above can be done with these commands:

```
git clone git@gitlab.com:drutopia-platform/drutopia_host.git
cd drutopia_host
git clone git@gitlab.com:drutopia-platform/hosting_private.git
git clone git@gitlab.com:drutopia-platform/build_artifacts.git
```

The contents of `drutopia_host` should look like this:

```
ansible.cfg
build_artifacts
host_vars
hosting_private
inventory_sample
provision.yml
README.md
roles
```

## Quick start:

You will have a deploy.log created automatically in the current folder. You may wish to occasionally move this out of the way as it will get rather large.

The commands here assume hosting_private is the current working folder.

`cd hosting_private`

Ensure you have connectivity and correctly configured your vault key, etc with:

`ansible -i inventory_live -m ping all`

Update the entire system (base software, all builds, all sites!). Diff is optional, but I like having it:

`ansible-playbook -i inventory ../provision.yml --diff`

Update a single member, using whatever builds already exist on the server:

`ansible-playbook -i inventory_live ../provision.yml --diff -e "target_member=family_home_test" --tags=members`

Individual builds can be updated and pushed to the server. Any builds marked not active will not build, but an individual one can also be targeted (i.e. skip all others) just like target_member:

`ansible-playbook -i inventory_live ../provision.yml --diff -e "target_build=stable" --tags="build,push"`

Configuration checks are performed prior to import. If a configuration was changed on the server, the site update will stop. To force it to continue, either config_import_force the option for the site, or force ALL sites to import config (recommed to use only when combined with target_member).
`ansible-playbook -i inventory_live ../provision.yml --diff -e "config_import_force_all=true,target_member=family_home_test" --tags=members`

## Primary tags

- build - perform a local build of a drutopia version
- push - place a build (must be built locally already) on the server. Does not deploy to any site.
- members - configure sites including creating user/database/pushing user source/activating site/performing drupal actions.
- setup - commonly skipped after an initial provisioning, as this wraps all the primary configuration of the server (install apache, mysql, etc)
(Add target build)
