About Drupal Distributions
==========================

.. epigraph::

   Distributions provide site features and functions for a specific type of site as a single download containing Drupal core, contributed modules, themes, and pre-defined configuration. They make it possible to quickly set up a complex, use-specific site in fewer steps than if installing and configuring elements individually.

   -- The `drupal.org page listing Drupal distributions <https://www.drupal.org/project/project_distribution?f%5B0%5D=&f%5B1%5D=&f%5B2%5D=drupal_core%3A7234&f%5B3%5D=sm_field_project_type%3Afull&f%5B4%5D=&text=&solrsort=iss_project_release_usage+desc&op=Search>`_

General Introductions
---------------------

* `The Beginners Guide to Drupal Distributions <https://www.ostraining.com/blog/drupal/distributions>`_.
* `Introduction to distributions <https://www.drupal.org/docs/7/distributions/introduction-to-distributions>`_ (drupal.org).

Technical Guides
----------------

* `Drupal 8 Distribution Development and Devops Workflow <https://2016.badcamp.net/session/drupal-8-distribution-development-and-devops-workflow>`_ (video from BadCamp 2016).

Case Studies
------------

* `Open Social, a Community and Intranet Solution <https://www.drupal.org/case-study/open-social-a-community-and-intranet-solution>`_.

Advanced Topics
---------------

* `Distributions and Install Profiles: The Challenge and the Glory <https://events.drupal.org/vienna2017/sessions/distributions-and-install-profiles-challenge-and-glory>`_ (video, DrupalCon Vienna 2017).

