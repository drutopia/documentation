Welcome to Drutopia's documentation
===================================

`Drutopia`_ is an initiative within the Drupal project based in social justice values and focused on building collectively owned online tools. Current focuses include two Drupal distributions aimed at grassroots groups, ensuring that the latest technology is accessible to low-resourced communities.

The code is open source, and `available on GitLab`_, as are `our issues`_.

.. _Drutopia: https://drutopia.org/
.. _available on GitLab: https://gitlab.com/drutopia
.. _our issues: https://gitlab.com/groups/drutopia/issues

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   quickstart
   quickstart-ddev
   communication


.. _goals:

.. toctree::
   :maxdepth: 1
   :caption: Goals

   manifesto
   points-of-unity
   goals
   roadmap


.. _end-user-documentation:

.. toctree::
   :maxdepth: 2
   :caption: Drutopia End User Documentation

   end-user-documentation

.. _building-and-styling:

.. toctree::
   :maxdepth: 2
   :caption: Site Builder and Themer Documentation

   building-and-styling-drutopia
   theming/theming-drutopia
   theming/making-subtheme-of-octavia

.. _organization:

.. toctree::
   :maxdepth: 2
   :caption: Community Organization & Structure

   decision-making
   working-groups
   hosted-drutopia-standards
   platform-cooperatives
   software-cooperative
   drutopia-partners

.. _contributing:

.. toctree::
   :maxdepth: 2
   :caption: Contributing

   contributing
   drutopia-code-of-conduct
   research
   development
   development/adding-module-dependency
   development/update-existing-install-into-dev-environment
   extension-criteria-and-candidates
   technical-guide
   new-content-feature
   extending-features
   development/add-user-permissions
   synchronizing-configuration

.. _distributions:

.. toctree::
   :maxdepth: 2
   :caption: Distributions

   about-drupal-distributions
   drutopia
   creating-a-distribution

.. _hosting-with-ansible:

.. toctree::
   :maxdepth: 2
   :caption: Drutopia Hosting with Ansible

   hosting-with-ansible
