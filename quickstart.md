# Quickstart

There are two quick ways to get Drutopia up and running: PHP on your local computer or a full, production-like environment provided by a virtual machine.

In both cases we'll end with the Drutopia distribution codebase, built by Composer, in a directory called `my-drutopia-site` (please change this to whatever you would where it appears in the instructions).

See the [DDEV virtual machine quickstart instructions](quickstart-ddev) (recommended), or continue below for local PHP instructions.

## PHP

### Prerequisites

1. PHP (7.3+, 8.1+ preferred; test in a terminal with `php --version`).
2. [Composer](https://getcomposer.org/download/).

> **Note**: If you have composer installed [globally](https://getcomposer.org/doc/00-intro.md#globally) you can use `composer` instead of `./composer.phar` in the first command.

### Instructions

Run these commands in a terminal

1. Install the code base using Composer:
    * `./composer.phar create-project drutopia/drutopia_template:dev-master --no-interaction my-drutopia-site`
1. Change into the project directory:
    * `cd my-drutopia-site`
1. Run a script to get the latest tested versions of all Drupal projects for the Drutopia release you're installing.
    * `./vendor/bin/composer-pin-apply web/profiles/contrib/drutopia/bin/drutopia-tested-global-pinnings.sh`
    * For more information, see [`web/profiles/contrib/drutopia/bin/README.md`](https://gitlab.com/drutopia/drutopia/blob/8.x-1.x/bin/README.md)
1. Change into the web root and start PHP's built-in server:
    * `cd web && php -S localhost:8008`
1. Open [localhost:8008](http://localhost:8008/) and follow the installation instructions; leaving everything at defaults will be fine.

See [the README](https://gitlab.com/drutopia/drutopia_template#readme) that was downloaded in that step for more.
