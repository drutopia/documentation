# Decision Making
Drutopia is a member-owned cooperative. All members have equal voting power to elect the Leadership Team, the ability to become a project maintainer, set the strategic goals of Drutopia and inform the technical roadmap.

## Membership

To become a member of Drutopia, a person must do to the following -
* Pay an annual membership due of $600 (or $50 a month)
* Agree to the Drutopia Code of Conduct


## Leadership Team

The Leadership Team can make strategic decisions regarding Drutopia. This includes vision, mission, goals, outreach strategy and target audiences, licensing. Decisions are made by consensus.


### Joining the Leadership Team

The current leadership team is:

* Ben
* Clayton
* Nedjo
* Rosemary

We are currently looking for a representative for front-end development. Open positions will be filled by consensus from the current leadership team.

Once the project has been established for a longer time a democratic process driven by Drutopia members at large will be implemented.

### Leadership Team Elections

Elections of the Leadership Team will happen annually, once membership is open to the public.


