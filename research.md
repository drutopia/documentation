# Research

We use the [Design Justice Network Principles](http://designjusticenetwork.org/network-principles/) in researching, designing and building Drutopia. 

"Design justice rethinks design processes, centralizing people who are normally marginalized by design and using collaborative creative practices to address the deepest challenges our communities face."

Here are the tools we've developed so far to help us conduct our research. 

## Survey

The survey exists at drutopia.org/survey

1. What is your name?
1. Describe the organization you work with.
1. What is your role in the organization?
1. What is your current annual budget for your website?
  1. Less than $1,000
  1. $1,000-5,000
  1. $5,000-$15,000
  1. $15,000-$30,000
  1. $30,000-$100,000
  1. Over $100,000
1. What is your current organization’s website built on (eg: Wordpress, Squarespace, Wix, custom Content Management System)?
  1. Drupal
  1. Joomla
  1. Nationbuilder
  1. Squarespace
  1. Weebly
  1. Wix
  1. Wordpress
  1. Other.  Please enter: [                     ]
  1. I don’t know
1. How is your website hosted?
  1. Shared hosting--name of hosting service if known
  1/ VPS
1. How satisfied are you with your current website?
  1. Scale: 1-5
1. What can you do well with your current website?
1. What is difficult/not possible to do with your current website?
1. How important is it that your website use free/open-source technology?
  1. Very Important
  1. Somewhat Important
  1. Not Important
  1. Unfamiliar with free/open-source technology
1. How important is that the technology tools you use be organized along democratic lines? (eg: tech cooperative, user-owned, etc.)
  1. Very Important
  1. Somewhat Important
  1. Not Important
1. Rate the following website features from most to least important to your work.
  1. Site analytics visualization
  1. Blogging
  1. Customer Relation Management (CRM integration)
  1. Customizable templates (for layout and visual design)
  1. Easy to add and manage content
  1. Events
  1. Mapping functionality
  1. On-site donations
  1. On-site fundraising campaigns (as opposed to needing to set up separate pages on GoFundMe, Funrazr, etc.)
  1. Photo Gallery
  1. Project/Chapter Directory
  1. Resource Library
  1. Strong Design
  1. Other
1. How interested would you be in combining efforts with similar nonprofits to fund the development of technology otherwise out of your price range?
  1. Very Interested
  1. Somewhat Interested
  1. Not Interested
1. What else should we know to build a user-owned platform that is relevant to nonprofits?


## Interview 

### Questions
These are prompts that can be used in an interview of potential members.  The goal is to validate or invalidate some of our assumptions about organizations' needs (see https://app.leanstack.com/canvases/245250 ).  Pain points that resonate with people can then be tried as [Landing pages] to see if it can speak to a larger number of people through the medium of a web page— a necessity to scale.  The goal with the potential member interviews, though, is to get people to open up and tell us about their organizations and their concerns.  This gets us much more valuable information than getting people to say yes or no to a particular technology solution.  (In the lean startup language, in particular from Ash Maurya's book Running Lean, this is the "problem interview" to understand our members' worldview. We're trying to mitigate product risk by seeing if people rank the problems we're trying to solve highly, market risk by seeing how people solve the problem today, and customer risk by seeing if the people who have the pain are willing and able to pay for solutions.)  We should also be sure to get all the answers for the [User survey] from people who agree to be interviewed, either before the interview as a qualifying process or afterward as a follow up.

### Potential prompts

1. What are your organization goals?
1. Who is it important that your organization reach for you to achieve your goals?
1. How do you try to reach them?
1. Are volunteers important to your organization meeting its goals?
1. How do you find volunteers?
1. If you don't use volunteers, why not?
1. Do you accept donations?
1. How important are donations to your organization?
1. Do you maintain a relationship with donors?
1. How do you thank donors and follow up with information?
1. How do you communicate with volunteers?
  * Text message
  * Email
1. How do volunteers commit to helping with work?
1. What do you need to tell your base?
1. How do you currently communicate this information to your base?
1. How do you maintain continuity of operations and institutional knowledge when staff or volunteers change?

## Interview structure

(From Running Lean, chapter 7)

### Set the stage

Thank you very much for taking the time to speak with us today.

We are currently working on a platform for web sites for grassroots organizations.  We got the idea for the service from seeing friends and clients unable to get the web site quality and capabilities they needed at a price they could afford.

But before getting too far ahead of ourselves, we wanted to make sure other organizations share these problems and see whether this is a platform worth building.

The interview will work like this.  I'll start by describing the main problems we are tackling, and then I'll ask if any of those resonate with you.

I'd like to stress that we don't have a finished product yet, and our objective is to learn from you, not to sell or pitch anything to you.

Does that sound good?

### Collect demographics (test customer segment)

### Tell a story (set problem context)

### Problem ranking (test problem)

(State the top three problems and ask your prospects to rank them- change order presented in different interviews.)

Do you have any other communication or web technology problems or pet peeves I didn't talk about?

### Explore member's worldview (test problem)

(This is the heart of the interview.  The best script here is "no script".  Go through each problem in turn.  Ask the interviewees how they address the problem today.  Then sit back and listen.  Let them go into as much detail as they wish.  Ask follow-up questions, but don't lead them or try to convince them of the merits of a problem (or solution).

### Wrapping up (the hook and ask)


As I mentioned at the start, this isn't a finished product, but we are building a platform that will give organizations like yours a better experience and more powerful communication tools.  The best way to describe the concept might be "SquareSpace for grassroots organizations" (replace SquareSpace with the name of the interviewee's existing service).

Based on what we talked about today, would you be willing to see the platform when we have something ready?

Also, we are looking to interview other people like yourself.  Could you introduce us to other leaders of grassroots organizations?

### Document results

(Take the five minutes immediately following an interview to document results while they're still fresh in your mind.  Have two people present for the interview, such as notetaker and interviewer, independently fill this out.  Then debrief and compare notes before entering into a system sharing the interview results with the rest of the team.  Be sure to capture the words people used to describe their organization and their problems.)

## Research Results

Below are the results from the research we have conducted so far.

### Showing up for Racial Justice (Denver)

The Denver chapter of Showing up for Racial Justice has been experiencing a boom in interest. As a volunteer-driven group, it has been challenging to channel the  energy of new participants meaningfully. Here are the user stories most important to them.

As a new supporter of SURJ, I want to know what action I can take right now, so that I can immediately do the work of racial justice.
As a new supporter of SURJ, I want to know what kind of ongoing volunteer opportunities are available, so that I can contribute in meaningful ways.

As a volunteer coordinator, I want to know what skills and interests a new volunteer has, so that I can plug them into the right working group.
As a volunteer coordinator, I want to know what experience a new volunteer has with racial justice, so that I can refer them to relevant resources for education.
As a volunteer coordinator, I want to know what work volunteers have followed through with, so that I can determine how reliable they are.



