
**Before** upgrading the site:

```bash
drush pmu quickedit rdf
```

*Note: Always try on local or test first in case these uninstalls cause a cascade of uninstalls.*
