# Contributing to Drutopia Documentation

[docs.drutopia.org](https://docs.drutopia.org) is hosted by [Read The Docs](https://readthedocs.org/) using [Sphinx](http://sphinx-doc.org) with [MyST](https://myst-parser.readthedocs.io/en/latest/index.html) to combine the best of ReStructuredText and [Markdown](https://commonmark.org/)

## Frequently Used Syntax

Here are some commonly used special features, but mostlcopy bits as needed from other 

### Admonitions

See [directives in MyST](https://myst-parser.readthedocs.io/en/latest/using/syntax.html#directives-a-block-level-extension-point) for an example of how to create arbitrary admonitions.

### Notes

Notes are 'admonitions' with a simpler syntax.  This code:

````
```{note}
As a common annotation, notes have a shorter syntax.
```
````

Produces this output:

```{note}
As a common annotation, notes have a shorter syntax.
```

## ReStructuredText

If you're a fan of ReStructuredText, you're in good company— it's Read The Docs' recommended format.  So long as you're willing to help others with the syntax from time to time, don't let us hold you back from flexing the power of making a `.rst` file.

Most people are much more comfortable with Markdown, and [MyST](https://myst-parser.readthedocs.io/en/latest/index.html) gives it enough power to write full-scale documentation, so that (files with `.md` extensions) is our default in this documentation.
