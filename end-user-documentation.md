# Using your Drutopia site

**Drutopia End User Documentation**

## Login/logout

Log in via the login link (located in the top, right hand region above the main menu) using your user name and password.

Note: If you do not have the user login block enabled, append `/user/login` to your website address (for example [demo.drutopia.org/user/login](https://demo.drutopia.org/user/login)) and you will get to the log in box.

If you ever forget your password, you can have a new one-time login link emailed to you.  Use it to log in and then re-set your password immediately.

Note: When using this one-time log in do not do anything else on the site before setting your password or you will be prompted for your old password which you do not have.

To log out, use the logout link located in the top, right hand region above the main menu on a standard Drutopia site.  Alternately, go `/user/logout`.

## Using the admin menu toolbar
* As a site manager (when you are logged with with manager permissions) you will see a toolbar at the top of the website.  This is a handy way to navigate to the areas you may need to access for site configuration and other administrative tasks.
* Clicking on the top level (e.g., Content, Structure) will take you to the administration page for that section.  So you can click on the Structure button and go to a page where you can select the appropriate tool within this section, for example block layout or taxonomy.
* The tool bar also has dropdown menu functionality, so you can also navigate directly by hovering over the main button and navigating directly to a subsection or even an item within that (for example to add a block by hovering over the Structure button, and then Blocks, leading to an Add custom block link).
* You can also navigate to the administrative sections of your website by entering the complete address, e.g., https://websitename/admin/structure/block.

## Roles and user accounts
* A key reason for using a content management system such as Drupal is to spread out the work of adding content to your site. Ensure that all staff members or volunteers who are in a position to contribute to the site have an account set up for them and are assigned the appropriate role for the tasks they will be carrying out on the site.

### Adding a new user account
* Start by logging in with a site manager account.
* Once logged in, with the manager role, you will see a toolbar at the top of the page.
* Select the “People” link.
* Start by clicking the button “Add user”.
* Type in a user name as well as an email and a password. Note that any email address may only be used for one user account.
* You can opt to inform a user of their new role (they will be sent a one time login link).
* Save the new user.
* The next step is to assign this new user to a role. Roles determine what permissions a particular user has (that is what they are allowed to do on the site.)

## User roles

The following roles are available on an Drutopia site:
 * __Anonymous user__: A site visitor who has not logged in (Exists by default).
 * __Authenticated user__: Anyone who has registered on the site, and authenticated by logging in. (Exists by default).
 * __Contributor__: A contributor is a member of staff or a volunteer who is going to be posting content but not editing other people’s content.
 * __Editor__: The editor can edit other people's content as well as post and edit their own.
 * __Manager__: The manager has permissions to add users and groups as well as some site configuration permissions.

```{note}
For a hosted version of Drutopia the highest permission level is site manager. For an independently managed Drutopia site, you would also have access to an administrator role which has all permissions on the site and hence has security implications.
```

On the main user administration page (`/admin/people`) you will see a list of users. Select the user you want to assign a role to by checking the box beside their user name. Then use the drop down selector under Action to choose the role you want to apply to the selected user. Then click the Apply to selected items button.

You can also remove roles from users this way as well as blocking and deleting users.


## Getting going with your new site

There is very little initial configuration that is required with your new Drutopia site. The following configuration is recommended:
* You should start by clicking the Configuration button in the toolbar, and selecting the Site information section (or navigate directly to `/admin/config/system/site-information`.) Here you can configure the site name, slogan, and site email address.
* If you did not do so during the install process, ensure that you have a default country selected. Under configuration, choose Regional settings.  If no default country is set, the calendars will often be off by one day.  Even if you set this during install, it is advisable to visit this page and re-save your settings to ensure proper calendar functioning.
* Also on the Configuration page, select Date and time, where you can change the display of the time settings as you wish for either AM/PM for 24 hour clock. Even is you are satisfied with the time/date displays, click the save button to ensure that correct displays for the calendar view.
* Configure any desired social media follow links for your site. Follow the link to the Social Media block (admin/structure/block/manage/socialmedialinks) and enter the URL for any service you wish to appear. Save your changes.
* After installing your site you may be prompted to Rebuild your content access permissions. Click the Rebuild permissions link.
* Configure your contact form (see below).
* Customize or delete the sample content (see below)

## Sample content

Drutopia includes sample content. This will make it even easier for users new to Drutopia and Drupal to get going with a new site. The sample content will give a visual snapshot of what your site will look like as it is filled with content, as well as providing pieces of content that can be edited to quickly get you started.
You can use the sample content in two ways:

1. For the piece of content you want to use, select the edit tab and then replace the title, other fields, body text and image with your own, and save making it real content for your site. As noted above, the sample content is created using a filtered HTML text format. You can switch that to use a WYSIWYG (what you see is what you get) text editor by selecting the desired text format below the body field.

2. Or you can use them merely as examples, reviewing how they are formed based on their various fields and then un-publishing or deleting them and starting fresh.

## Creating new content

### Selecting the appropriate content type

The first step in creating a new piece of content for your site, is deciding what content type is most suitable.

A Drutopia website may include the following content types (depending on which features you have enabled):

 * __Action__: An action is a specific, single action a user can take.
 * __Article__: Use articles for time-sensitive content like news, press releases or blog posts.
 * __Basic page__: Use basic pages for your static content, such as an 'About us' page.
 * __Blog__: Use blog for personal or journal-like posts.
 * __Campaign__: A campaign includes background information as well as ability to list demands and updates.
 * __Event__: Events have a date and time, an event type, and all the usual Drutopia fields (title, image, description, topic, and tags).
 * __Landing page__: Landing pages can be used for custom pages such as the home page.
 * __People__: Use people content type for people such as staff, volunteers, contributors.
 * __Resource__: A resource can be either a file, such as a PDF, or a link, such as a website URL or an embedded video.

It is helpful to pay attention to the content type and use the same content type for similar posts to ensure consistency.  For example, many posts will be displayed in different parts of the site based on the content type.
* Each content type may have slightly different fields to be filled in but there will be many similarities.

### Step-by-step instructions for adding content

Using the top of page toolbar, select “Add content” from the dropdown menu “Content”. You will see a list of content types to select from.  For example if you choose “article” you will get a form to fill in with the following elements:

  * __Title__: Give your post a title. This is a required field.
  * __Article type__: You can choose to categorize your articles by type. Possible types include news (comes by default) and any other article type terms you choose to add. (See taxonomy section below.) Other content types also have a “type” field.
  * __Authors__: Article and blog content type allow you to link the content to any author that has a ”people” entry. This field uses an autocomplete, so you start by typing in the person’s name. You will then see suggested matches. Click on the match to complete. Should you wish to add more than one author, click the “Add another item” button and repeat the autocomplete process.
  * __Image__: Click the “Browse” button to begin to add an image.  To upload a new image, first browse for the image and then select. Allowed image types are: png, gif, jpg and jpeg. The recommended image size is 1200 by 675 pixels. The images that accompany posts are used in numerous displays so it is highly recommended that you include an image wherever possible.
  * __Summary__: The summary should be a one to two sentence summary of your post. This will appear in various displays on the site. This is a required field.
  * __Body paragraph__: The body paragraph field is where the main content of your post will be placed. By default, a text paragraph will be open for you to begin typing in your content.
     * A text editor is installed to help with formating. Different role types have access to different levels of input options for formatting. You only have access to the level of formatting options to which your assigned role grants you access for security reasons.

     * Ideally text should not be pasted directly in from Word as it tends to create problems.  If you have already word processed your content, copy it from your document and paste it into a text editor to clear formatting before pasting in.
  * You can also add image or file paragraphs to add additional images or to upload file documents, such as PDFs. The article and basic page content types also have a video paragraph to let you add an embedded video (that is a video hosted elsewhere such as YouTube or Vimeo).
  * You can use the “drag and drop” arrow tool to rearrange your paragraph order.
  * __Topics__: Topics allow you to categorize your content across a number of specified content types.
  * __Tags__: Tags allow content to be sorted by terms that describe the content.  Using tags on posts will provide website visitors alternate ways to locate content. Enter tags as a comma-separated list.

There are a number of other collapsed items which can all be opened to reveal further input options if you have the required level of permission.  Most can just be left at their defaults for the particular content type which have been pre-configured for the most likely setting.  But if you want to change any of those settings you may do so.  The most likely ones you may need to change would be:
  * __Revision log message__: In the future, when editing  a post you can log the changes made.
  * __Menu settings__: For most posts, you will not need to do anything under menu settings, but if for example you want an item to appear in the menu system, you can check the “provide menu link” box and configure the menu links. (More on menus below.)
  * __Comment settings__: Can be changed from open to closed.
  * __URL path settings__: Post are automatically giver a URL alias which is a user friendly path. If you would like to manually provide a path, you can uncheck the “Generate automatic URL alias” box and enter your own path.
  * __Authoring information__: If you need to change the author or date, do so here.
  * __Publishing options__:
    * Promoted to front page (this promotes a piece of content to the home page, is the default for the article content type.)
    * Sticky at top of lists (makes the piece of content “stick” as the top piece of content. Only one piece can be so marked at the time.  You will need to “unsticky” a piece of content before you “sticky” another one.

At the end of the form you may click the “Preview” button to see how it will look or just go ahead and click the “Save and publish” button. There is also a “Save as unpublished” drop down option on the Save button for work that you want to save but needs review before publishing.


## Specialized paragraphs

There are some specialized paragraphs that allow content editors to do more sophisticated work.

### Storyline

A storyline is a simple way to show a chronology or tell a story in a graphical format. A storyline can include specific dates but unlike some timelines does not require this, allowing you to organize in any manner you like.

* To use the storyline feature, you'll need to enable both Drutopia Storyline as well as Drutopia Page Storyline.
* Once these two features are enabled, the basic page content type will have an additional Storyline field.
* The initial body paragraph field provides a place to add opening text that will display at the top of your storyline.
* The storyline header paragraph contains a single field for a header, such as a year. This is open by default to start you off.
* Next, add a storyline item paragraph by clicking that button. This paragraph will have two fields, a heading where you can add a more specific date and a text field where you add the text you want to appear in your storyline.
* Continue to add storyline items which fit under one header, adding new headers as desired.
* You can rearrange using the drag and drop tool as needed and then save your page.

### FAQ

The FAQ paragraph type is available on both page and article content types.

* You can create a whole FAQ page by adding a series of FAQ paragraphs which each contain a question and answer (or title and information).
* Alternately, you can insert an FAQ paragraph into an article or page as a way of providing more information on a subject but that can be opened or closed by a site visitor.

## Editing content

It is very easy to go back into a piece of content you have created and make any changes required.

* When you are logged in, and if you have appropriate permissions, when viewing a piece of content you will see an edit tab on the top of the piece of content. By clicking that tab you will return to the form on which you created the content.
* You can also access content for editing via the content administration page described below.
* Simply make any changes as you did when you were first creating the piece of content and save those changes.


## Content administration

To view a list of all content that exists click the “Content” button on the toolbar.  This is a valuable location for site managers in managing the content on the site, with tabs for Content and Comments.
* Under the content tab, you can filter by content type, allowing you to search for all events for example.
* You can also filter by status: published or promoted.
* To update any piece of content, you can check the box next to it.  Then, select the update option you want (for example, _Promote content to front page_) and click the *Apply to selected items* button.
* Click the edit link to go to a particular piece of content directly in edit mode.
* Using the Comments tab you can approve or delete comments posted on your site.


## Home page setup

The home page consists of a number of elements. It can include:

* A home page hero image with accompanying text about your organization and a link to your About page.
* The latest four articles that have been promoted to the home page.
* The latest four actions that have been promoted to the home page.
* A home page featured block with an image, text and link to any page on your site.
* The latest four resources that have been promoted to the home page.
* The latest four blog posts that have been promoted to the home page.
* The latest four campaigns that have been promoted to the home page.
* Three custom blocks that appear at the bottom of the page and can link to any internal or external page.
* If your site does not have a particular feature enabled or there is no content of that type, the block will not appear.


## Editing the home page blocks

There are five custom blocks that appear on the home page (two wide slides, and three smaller ones), each containing an image, some text and a link.

* The easiest way to edit is to use the edit tool (with a pencil icon) that appears when you hover over the top right-hand corner. Click the edit button and then select Edit.
* Start by typing in the page on the site you want your home page block to lead to. For the home page hero block (at the top) this will likely be an “About” page. For the other home page block, it can be to any page that you are featuring.
* You can also edit the Link text (which by default will read “Learn more.”
* Remove the default image and upload your own image. Ideally the images for the homepage block should be 1500px by 750px. You will need to take care in selecting the images for these blocks.
* Edit the text that will appear over the image. This should be one or two sentences.
* Save your changes.
* Visit the home page to see how your new block looks. It is very likely that you will need to make further changes to ensure the text is visible with the image you have selected and not too long.
* The three bottom region blocks can be edited in a similar way, but for these it is the link text that will appear as in a title and the text should be extremely brief.


## Menu

By default certain menu items will appear in the main menu of a Drutopia site. These will vary depending on which features you have enabled.

* By default, main menu items are grouped into About Us, Our Work and Get Involved.
* Under these headings, the menu links appear as drop downs.
* To change menu links, go to menu section (under Structure).
* The main menu is called Main navigation (admin/structure/menu/manage/main).
* Here you can edit existing links or add new ones. You can also disable menu items you do not wish to appear by unchecking the Enabled box. You can also rearrange the order of menu items by dragging them. Ensure you save your changes.
* The main menu links are repeated (in expanded form) in the footer region, for ease of navigation.
* There is also a footer menu (admin/structure/menu/manage/footer) which includes links Contact and Privacy Policy. These can be edited in the same way as the main menu described earlier.
* A menu always needs a path which can be found for any piece of content or section in the address bar when you are viewing that piece of content (just what comes after the site name).


## Themes

Drutopia ships with a default theme (which determines how your Drupal site looks) called Octavia (in honor of [Octavia Butler](http://octaviabutler.org/)). Octavia has been built as a subtheme of Bulma using  the Bulma framework.
* Click on the settings page for Octavia (admin/appearance/settings/octavia) to adjust any settings (note that editing the Global settings will not work, you must edit the Octavia settings).
* You can upload your own logo and/or favicon.
* As always, ensure you save your configuration.

### Skins for Octavia theme
* As of the Beta3 release of Drutopia (August 2019), the Octavia theme includes a number of skins that change the appearance of your site.
* To change your site’s appearance using skins, navigate to the setting page for Octavia (admin/appearance/settings/octavia).
* Here under the heading Skins you will see the available skins with a description and screenshot.
* Select your skin and save your configuration.

## Taxonomy

The taxonomy system is a way of categorizing content. A vocabulary is created and within that vocabulary terms are added.

* The topics vocabulary is available for a sub-set of Drutopia content types. Topics are used to categorize content across multiple content types and terms should be well-thought out to capture the key issues or topics that will help your site visitors explore content.
* The tags vocabulary is created and available for a variety of Drutopia content types. Terms can be added to this vocabulary “on the fly” as you are creating pieces of content.
* Some content types have a “type” field, such as Article type, which allow you to categorize by a defined set of terms. So for example, for event, you may want to categorize as meeting, fundraiser, workshop etc.
* To add terms to a vocabulary, select taxonomy from under the structure button in the toolbar (admin/structure/taxonomy). Choose the vocabulary you want to work with and select the add terms button. Add a new term and save. Or edit the name of an existing term, should you wish it to be slightly altered.

## Related content

You can enable the Related Content feature to have related content show on the bottom of a node page.  Content is related based on terms that both pieces of content share in common.


## Contact form

The Contact form is enabled by default. You can personalize the email recipient and a return message at `admin/structure/contact/manage/feedback`


## Drutopia Groups

Groups allows you to create sub-sections on your site for groups that can be defined in a number of ways. Groups could be chapters or branches of your organization. Or they could be used to group users together who might have shared interests.

* Only basic group functionality has been created for Drutopia as further customization will be provided by the Solidarity distribution which is being built off of Drutopia.
* However, if you’re starting with Drutopia a site administrator can add some of the more complex functionality with a small amount of configuration, such as linking groups to particular content types.
* To work with groups, navigate to the groups admin page via the toolbar. Here you can edit an existing group or add a new group. As well as similar fields that other content types have, groups have fields for contact information such as a phone number, email and address.

## Search

Site search includes all content types except for Landing Pages. If you want a page to end up in site search, use the Basic Page content type instead.

Leaving feedback
----------------

Feedback on the software and platform is welcomed. When leaving feedback, keep the following in mind -
* Be direct, yet kind with feedback.
* For bugs, state what is not working. See [Bug Reports/Testing](contributing.html#bug-reports-testing).
* For features, form this as a user story. See [Feature Requests](contributing.html#feature-requests).
* For how  questions, form these as a question.

If you are a platform member, submit feedback (bugs, features, how tos) in your GitLab project. Otherwise, feedback should be left in the general [Drutopia project](https://gitlab.com/drutopia/drutopia).
