Drutopia Base Distribution
==========================

[Drutopia Base](https://www.drupal.org/project/drutopia/) is the flagship libre software product of the Drutopia initiative. It provides the building blocks for the features grassroots groups need to organize, inspire, and mobilize.

Features
--------

Drutopia Base comes with:

### [Drutopia Action](https://www.drupal.org/project/drutopia_action/)

Provides the Action content type and related configuration. Use "actions" to post a specific, single call to action people are asked to take.

If the action takes place at a link, the one main link for the action will be formatted as a button.

### [Drutopia Article](https://www.drupal.org/project/drutopia_article/)

Provides the Article content type and related configuration. Use articles for time-sensitive content like news or press releases.

### [Drutopia Blog](https://www.drupal.org/project/drutopia_blog/)

Provides Blog content type and related configuration. Use blog for personal or journal-like posts.

### [Drutopia Campaign](https://www.drupal.org/project/drutopia_campaign/)

Provides Campaign content type and related configuration. A campaign includes background information as well as ability to list demands and updates.

### [Drutopia Comment](https://www.drupal.org/project/drutopia_comment/)

Provides comments and related configuration.  By default comments can be posted on blogs and articles (FACT CHECK).

### [Drutopia Event](https://www.drupal.org/project/drutopia_event/)

Provides Event content type and related configuration. An event contains a date.

### [Drutopia Group](https://www.drupal.org/project/drutopia_group/)

Provides a basic group type that can be classified by a group type vocabulary.

Each group can post its own content (like news articles, events, actions)

This feature is under development.

### Drutopia Home Page

Provides a home page for Drutopia.

### [Drutopia Page](https://www.drupal.org/project/drutopia_page)

Provides the *Page* content type and related configuration. Use basic pages for your static content, such as an 'About us' page, or for landing pages, such as a custom home page.

You can give pages a type (such as "Basic page" or "Landing page") which will not, by default, be shown when viewing the page, but can be used to filter your pages in administrative or search listings.

### [Drutopia People](https://www.drupal.org/project/drutopia_people/)

TODO: Rename to person.

Provides People content type and related configuration. Use people content type for showing visitors information about people such as staff, volunteers, contributors.

This is distinct from Drupal user accounts.  It is used for public-facing profile pages, for people who may or may not have a user account on the site.

### [Drutopia Related Content](https://www.drupal.org/project/drutopia_related_content/)

Provides related content on node pages generated via similar terms.

### [Drutopia Resource](https://www.drupal.org/project/drutopia_resource/)

Provides *Resource* content type and related configuration. A resource can be either a file, such as a PDF, or a link, such as a website URL.

### [Drutopia Search](https://www.drupal.org/project/drutopia_search/)

Provides search functionality, indexing, and an overall search listing view.

### [Drutopia Social](https://www.drupal.org/project/drutopia_social/)

Provides a social media block.

### Drutopia Storyline

Provides Storyline paragraphs.

### Drutopia User

Provide User related configuration.

### [Octavia theme](https://www.drupal.org/project/octavia/)

A [Bulma-based](https://bulma.io/) theme which is the default base theme for Drutopia sites.  Some Drutopia features, such as Indieweb microformats markup, will go into this theme and have to be re-implemented in any other theme used for Drutopia.

The [Skins module](https://www.drupal.org/project/skins) makes it easy to have variations on a theme without having to re-do block placement or create a whole new theme.  Octavia comes with several high-quality skins optomized for Drutopia.  See [Octavia Camouflage](https://www.drupal.org/project/octavia_camouflage) for a grab-bag of skins used for specific Drutopia sites that are not generalized enough to use directly for other sites.

Roadmap
-------

See [Drutopia milestones on GitLab](https://gitlab.com/groups/drutopia/-/milestones/) and (less specific but more up-to-date) [all issues](), including [wishlist issues]().
