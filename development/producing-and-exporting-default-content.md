# Producing and exporting default content

Default content is created and imported using the [Default content](https://www.drupal.org/project/default_content) module.

## Prior to working on default content

* Ensure the following modules are installed;
  * [Default content](https://www.drupal.org/project/default_content).
  * [Better Normalizers](https://www.drupal.org/project/better_normalizers). This allows files such as images to be exported into the export text files.
* Ensure you have Drush installed.
* If the feature you're working on already has default content, ensure you have the latest version installed. This ensures you can edit the existing default content.

## Creating default content

* Log in as the user with UID 1. Note: at a later point we may switch to using a different user for default content.
* Create content to be exported. Most or all content entity types (nodes, taxonomy terms, custom blocks) are supported.
* If you are adding any images or other files, pay attention to:
  * _File size_. Files that we add contribute to overall distribution bandwidth and download size. Also, we may need to edit exported files (for example, to remove the file ID, see below) and the file itself is serialized into the exported text file, meaning that very large files may present issues when opening in a text editor. Prior to uploading files to the site you're generating demo content on, resize the file. For standard images, resize to 1200px by 675px. For slide images, resize to 1500px by 750px. For people images, use 600 px by 800 px.
  * _Licensing_. All default content must be GPL licensed. We cannot include, for example, non-GPL Creative Commons licensed items. The simplest approach is to stick to work for which you own the copyright and are willing to grant GPL licensing.

## Exporting default content

Default content is exported into a "content" directory.

* Open a terminal and change into the root directory of the website. Note: the drush command will not work in any other directory.
* Determine the entity type and ID of the item you wish to export. For example, if it's a node, got to the edit form and look in the URL to find the node ID.
* Run the command `drush dcer [entity type] [entity ID] --folder="path/to/myfeature/content"` where [entity type] is the entity type and [entity ID] is the entity ID and path/to/myfeature is the path to the feature you wish to add the default content to. For example, `drush dcer node 9 --folder="modules/contrib/drutopia_article/content"`. This command exports the specified entity _and_ all related references, such as taxonomy terms, paragraphs, and so on.
* For blocks, the entity type is `block_content`.
* For menu items the entity type is `menu_link_content`.
* Delete the exported directory 'user'. This is because we don't need to export the user with UID 1. Note: there is an [open issue](https://www.drupal.org/node/2815051) on the Default Content module that would prevent export of this user.
* If there is a `taxonomy_term` directory, determine whether any of the terms included in that directory are from vocabularies that are provided by a module other than the one you're adding the default content to. If so, they will need to be moved to that other module. For example, if there's a term exported from the `tags` vocabulary, move this from its exported location to the `drutopia_core` module, in the `content/taxonomy_term` directory, alongside other tag terms. Not sure if there's a term from the `tags` vocabulary? You can recognize it by looking through the export of such a term for a section like this ("tags" here is the vocabulary name of the term):
```
    "vid": [
        {
            "target_id": "tags"
        }
    ],
```
* Edit each of the exported files to remove integer ID, revision ID, and target revision ID values. This step is done to prevent ID conflicts when the content is imported/created. See [this commit](https://gitlab.com/drutopia/drutopia_event/commit/fccc1ee83e1c745cf03cdccbc711f7c54da07136) for an example of which specific keys need to be deleted. They include:
    * For files, the `fid` key. For example, if the file ID was 65, you would delete these lines:

    ```
        "fid": [
            {
                "value": 65
            }
        ],
    ```
    * For groups, `id`.
    * For nodes, `nid` and `vid`.
    * For paragraphs
      * `id` and `revision_id`, `parent_id` and `behavior_settings`.
    * For taxonomy terms, `tid` values.
    * For nodes (referencing paragraphs), `target_revision_id` values.
* Also edit each of the exported files to remove `"metatag"` values.
* After making all these manual edits, it is helpful to run your .json files through a .json validator such as https://jsonlint.com.
* `git add` the "content" directory and its contained files.
* Note: you do not need to add a dependency on `default_content` or `better_normalizers`, nor do you need to add these to your module's `composer.json` file. The dependency and composer integration will be done in the install profile. That way, default content is optional--if the default_content module is enabled, the default content will be installed.
