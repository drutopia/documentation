### Update an existing install into a development environment

If you want to do development on a bunch of Drutopia feature modules in an environment that was installed with something like `composer create-project drutopia/drutopia_template:dev-master --no-interaction DIRECTORY` (where DIRECTORY is whatever your directory is called) then you can follow these instructions.

```{note}
As per the [development documentation](/en/latest/development.html) the recommended approach is to use [Drutopia Dev Template](https://gitlab.com/drutopia/drutopia_dev_template).
```

When you've started with the release versions of Drutopia features so you should run the following script to replace those with git repositories before installing.

```
# Name this file fix_directories.sh and place it in the root directory of your
# Drutopia install (parallel to the vendor and web directories).
# Adjust file permissions so the file is executable.
# From a terminal in the directory you placed the file, run:
# ./fix_directories.sh
cd web/modules/contrib/
echo "Fixing Drutopia modules"
for dir in "drutopia_"*
do
  echo "Fixing $dir"
  rm -fr $dir
  git clone "git@gitlab.com:drutopia/$dir.git"
done
cd ../../profiles/contrib/
echo "Fixing drutopia install profile"
rm -fr drutopia
git clone git@gitlab.com:drutopia/drutopia.git
cd ../../themes/contrib/
echo "Fixing octavia theme"
rm -fr octavia
git clone git@gitlab.com:drutopia/octavia.git
cd ../../../
```

To use the script:

* Create a new file, `fix_directories.sh`, and put it in the root directory of your site.
* Adjust file permissions on `fix_directories.sh` as needed to add execute access.
* From the directory containing `fix_directories.sh`, run the following at the command line: `./fix_directories.sh`


* Visit your site and follow the Drupal installation instructions.
* Log into your new site and install the modules needed for development. These include:
 * All Drutopia feature modules (which are listed on the modules page under the heading "Drutopia").
 * Features UI and its dependencies (Features and Configuration Manager Base).

#### To update such an environment

It can be time consuming to pull in upstream changes for each of the many Drutopia features you're working with. The following shell script can help.

```
#!/bin/bash

# store the current dir
CUR_DIR=$(pwd)

# Let the person running the script know what's going on.
echo "Pulling in latest changes for all repositories..."

# Find all git repositories and update it to the master latest revision
for i in $(find . -name ".git" | cut -c 3-); do
    echo "";
    echo $i;

    # We have to go to the .git parent directory to call the pull command
    cd "$i";
    cd ..;

    # finally pull
    git pull --rebase origin 8.x-1.x;

    # let's get back to the CUR_DIR
    cd $CUR_DIR
done

echo "Complete!"
```

To use the script:

* Create a new file, `update-repositories.sh`, and put it in the parent directory of the directory of your Drutopia feature-module repositories.
* Adjust file permissions on `update-repositories.sh` as needed to add execute access.
* From the directory containing `update-repositories.sh`, run the following at the command line: `./update-repositories.sh`
