# Adding user permissions

## Overview

The [Config Actions](https://www.drupal.org/project/config_actions) module allows you to alter existing configuration. We're using it to add permissions to roles. We do so because the role needs to be provided by one feature (usually, `drutopia_core`), while permissions need to be provided by various other features, such as a feature that provides a content type.

### Technical details

It works on the basis of plugins, each of which applies a different sort of action. One of the plugins is `add`, which will add to an existing configuration item.

The `path` you specify answers the question "what specific parts of the configuration do you want to change?" In our case, the path is `permissions` because that's the part of the user role we want to change.

Want to know more about how Config Actions works? There is [extensive documentation available](http://config-actions.readthedocs.io).

### How to add permissions

* Configure your site and add the permission(s).
* In the feature you're working on, look for a directory called `config/actions`. If it does not already exist, create it.
* Edit the `composer.json` file and, if it doesn't already exist, add the line `    "drupal/config_actions": "^1.0-beta1",` to the `require` section.
* Edit the `.info.yml` file and, if it isn't already there, add `  - config_actions` to the dependencies section and ensure that `drutopia_core` is also there as a dependency.
* For each role that you added a permission to:
    * Determine if there is already a file `config/actions/user.role.[role_name].yml`, where [role_name] is the machine name of the role. If not, create it, using the following for the file content:

        ```
        path:
          - permissions
        plugin: add
        actions:
        ```

    * On the site, use the admin UI to export the role you added the permission to. You do this by selecting "Configuration > Development > Synchronize configuration > Export > Single item" (admin/config/development/configuration/single/export). For "Configuration type", select "Role" and then for "Configuration name" select the role you're exporting. An export of the role will appear in the form. Here is an example:

        ```
        uuid: 19da04f2-c4c7-4f34-b47e-f52e2bc315fc
        langcode: en
        status: true
        dependencies: {  }
        id: manager
        label: Manager
        weight: 3
        is_admin: null
        permissions:
          - 'access administration pages'
          - 'access toolbar'
          - 'administer thingumajigs'
        ```

        Under `permissions`, you get the machine names of all permissions assigned to the role. Look for the ones you assigned (note: the machine names may differ from what you saw through the UI). For each permission you're adding, add a line like the following to the user role file in `config/actions`.

        ```
          '[permission name]':
            value: '[permission name]'
        ```
        
        where [permission name] is the machine name of the permission. For example, your file might end up like this:

        ```
        path:
          - permissions
        plugin: add
        actions:
          'administer thingumajigs':
            value: 'administer thingumajigs'
        ```

### Troubleshooting

If, after adding permissions with the config actions approach, you get the following cryptic message when doing a site install:

>    In ConfigEntityStorage.php line 252:
>
>      The entity does not have an ID.

It's quite possible you forgot to add or enable a module that provides the role (or possibly other related configuration).
