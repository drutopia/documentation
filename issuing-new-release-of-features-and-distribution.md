# Issuing a new release of the Drutopia base distribution

Here are the steps for issuing a new release of Drutopia's main distro.  Other Drutopia-based distributions should work similarly.

## Set up hackysync

To be able to push changes either for feature modules or the distribution, we use a set of scripts called hackysync. See the [`README`](https://gitlab.com/drutopia/hackysync/blob/master/README.md) in the [`hackysync`](https://gitlab.com/drutopia/hackysync) repo.

Once you've cloned the repository, run some setup before doing either feature module or distribution updates:

    cd hackysync
    git pull
    ./get_all_repositories.sh

## Issue new feature module releases if needed

We need new releases of any feature that has had changes made.

* Ensure updates for all projects are pushed to GitLab and drupal.org (and are up-to-date on your local).
* Determine which, if any, Drutopia feature project has changes since the last release:
    ```
    cd hackysync
    ./update_drupal_org_from_gitlab.sh
    ./commits-since-most-recent-tag.sh
    ```
* For each Drutopia feature project with changes since the last release:
    * Use the hackysync script to create and push a new tag. Example: `./tag_project_release.sh drutopia_article 1.0`.
    * Post a new release on drupal.org. See [relevant documentation](https://www.drupal.org/node/1068944).

### Document

* Update `drutopia/composer.json` requirements so that each Drutopia project references the latest release.  Get an overview of current releases with `./current-tags.sh`.  (This isn't necessary technically but is hugely useful in understanding what has changed between releases.)
* Paste the results of the automated check of changes since last stable release (from above, don't re-run) into https://gitlab.com/drutopia/documentation/edit/master/drutopia-releases.md 

## Issue a new release of the distribution

If you only want to update the distribution, for example, for a security release of a contributed module or Drupal core, start here.

### Update pinned dependency versions

We include a file in the install profile that records the specific versions of all Drupal projects included in a release. Prior to posting a release, we need to update this file. By doing so, we make sure that anyone who installs the release can make sure they have exactly the same versions of all Drupal code as the ones we tested.

* Generate the full code base:
    * Start in a directory where you can install a website. If you're developing locally, this might be `/var/www/html`.
    * Run `composer create-project drutopia/drutopia_template:dev-master --no-interaction my-drutopia-test`
    * Remove the `composer.lock` file.
    * `cd my-drutopia-test`
    * Then run `composer require drutopia/drutopia:dev-8.x-1.x` to get the current dev version of the `drutopia` install profile.
* In the same site directory, e.g. `my-drutopia-test`, run a composer command, `composer show drupal/* > web/profiles/contrib/drutopia/bin/drutopia-tested-global-pinnings.sh`. This command:
    * Lists all Drupal projects in the version actually installed.
    * Writes the result into a file in the `drutopia` install profile, `bin/drutopia-tested-global-pinnings.sh`, replacing the previous contents of that file. You'll find this file at `my-drutopia-test/web/profiles/contrib/drutopia/bin/drutopia-tested-global-pinnings.sh`
* Don't yet commit the results. We need to test first.

### Test

* Install a fresh Drutopia site using the code base you generated in the previous step.
* Test.
* If testing is successful, commit the change to `bin/drutopia-tested-global-pinnings.sh` in the `drutopia` install profile.
    * Start in `my-drutopia-test`
    * `cd web/profiles/contrib/drutopia`
    * `git add .` or `git add -u`
    * Optional: change the remote to use SSH rather than HTTPS so you can commit with your SSH key rather than a name + password
        * `git remote set-url origin git@gitlab.com:drutopia/drutopia.git`
    * `git commit -m "Update pinnings to latest stable releases"` or a similar message as appropriate
    * `git push`

Note: the `drutopia` reposity will have been cloned using HTTPS rather than SSH. This means that you can't use your SSH key as a credential for pushing to GitLab. Instead, you'll be prompted for your gitlab.com user name and password.

### Set release version

* Edit the `drutopia` install profile's `.info.yml` file to specify the version string for the release you're about to post.
* Commit.
* Push.

### Push changes

Run the hackysync scripts in order to push latest changes to install profile to drupal.org. The script will first pull in the latest changes from GitLab, including the commits you just made.

If you have not already, do the `hackysync` setup listed above.

Then:

    cd hackysync
    ./update_drupal_org_from_gitlab.sh

### Issue a release of the Drutopia distribution

* Add an `8.x-1.0` tag and accompanying `1.0` tag and push them to GitLab and drupal.org repos. This can be done using the hackysync script: `./tag_project_release.sh drutopia 1.0 distro`. Note that the third argument - in this case, "distro", though in fact it could be anything - is what triggers the script to add the short version tag `1.0-alphaXX`. This is needed when packaging the `drutopia` project but not when packaging regular feature-type projects such as `drutopia_article`.
* On Drupal.org, post a release for the Drutopia project.

### Post-release steps

* Edit the install profile's `.info.yml` file, returning the version to 8.x-1.x-dev and commit.
* Update the `composer.json` file in the `drutopia_template` to use the new release version and commit.
* Checkout the drupal8 branch and make the same change and commit.
