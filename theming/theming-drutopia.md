# Theming Drutopia

```{admonition} New!

We highly recommend **[choosing](https://www.drupal.org/docs/contributed-modules/skins) or [creating](https://www.drupal.org/docs/contributed-modules/skins/theme-developer-usage) a [skin](https://www.drupal.org/project/skins)** *instead* of creating a new theme.  Check in the Octavia theme's settings for several available skins.
```

Drutopia supports being themed with all the flexibility and power of any Drupal site.  Consult the [Drupal 8 theming documentation](https://www.drupal.org/docs/8/theming) for more information.

The Drutopia distribution itself comes with the Octavia theme, a sub-theme of [Bulma](https://www.drupal.org/docs/8/themes/bulma).  Because Octavia includes templates corresponding to Drutopia's features, and because the Bulma CSS framework it relies on is pretty great, we recommend [building your Drutopia theme as a sub-theme of Octavia](making-subtheme-of-octavia.md).

```{note}
Why is the Bulma CSS framework pretty great?  The lightweight, configurable approach to styling web pages is based on [Flexbox [title="CSS Flexible Box Layout]](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout) and fully responsive to varying screen sizes.  It's relatively easy to learn with a simple syntax requiring minimal HTML, but also highly customizable through the (syntatically) awesome power of [Sass [title="Syntactically Awesome Styleshets]](https://sass-lang.com/).
```

Still, Bulma is only one of many possible approaches.  We would love to see your preferred framework (Bootstrap, Zurb Foundation, etc.) adapted to Drutopia, and would especially be eager to help you create a Drutopia-ready base theme to give people a head start in styling their Drutopia sites with other CSS frameworks.  (As Octavia serves as a base theme for Drutopia but is itself a sub-theme Bulma, a logical starting point for a new base theme for Drutopia use would be creating a sub-theme of [Bootstrap](https://www.drupal.org/project/bootstrap) or [Zurb Foundation](https://www.drupal.org/project/zurb_foundation).)  Please [get in touch](communication)!

If you plan to [style Drutopia with Bulma by creating a sub-theme of Octavia, please read the documentation as there's plenty of quick hints that will save you lots of time](making-subtheme-of-octavia.md).
