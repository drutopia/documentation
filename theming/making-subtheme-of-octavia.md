# Making a subtheme of Octavia (using Bulma to style your Drutopia site)

In this tutorial, we'll create a subtheme of the Octavia theme and use it to produce a customized version of Bulma.

Recommended reading: the [Customize](https://bulma.io/documentation/customize) section of the Bulma documentation. We'll be using `node-sass`, so if you like you can skip the other two options covered there (Sass CLI and webpack).

In this example, we'll create a new Drupal theme called `calla`.

## Caveats

* Subtheming, while supported and encouraged in Drupal for a long time, still has edge cases that can cause problems.  For example, Drutopia is [applying a patch to fix Display Suite templates](https://gitlab.com/drutopia/drutopia/issues/206).  The only known pitfall is that overriding parent theme library assets is far more brittle than it ought to be.  This guide will walk through that— but of course it's the unknown pitfalls that we have to worry about.

## Documentation for setting up a local development environment

### Get the code

If you have a Drutopia project working already, clone your project's repository.  If not, start fresh by getting Drutopia running locally using the [method of your choice](../quickstart.md).  For this documentation we will presume you are using a DrutopiaVM-based project:

* Clone the [Drutopia VM repository](https://gitlab.com/drutopia/drutopia_vm).
* Follow the instructions in the README.md file in the root of the repository to get the VM up and running.

### Base install

* When you first open up the [`http://drutopia-vm.local`](http://drutopia-vm.local) site, install Drutopia.

## Set up for theme development

* Enable Twig debugging. See the tips [here](https://www.drupal.org/docs/8/theming/twig/debugging-twig-templates). You'll need to change the permissions on the `sites/default` folder to allow creating the `services.php` file.

## Create a new theme based on Octavia

### Create a new directory

Create a directory with the name of your theme, `calla`. Typically you create a `themes/custom` directory and create your new directory there.

### Create a `*.info.yml` file

The info file identifies your theme. For details, see the drupal.org documentation page [Defining a theme with an .info.yml file](https://www.drupal.org/docs/8/theming-drupal-8/defining-a-theme-with-an-infoyml-file).

In the theme's directory, create a new file called `themename.info.yml`. In our case, that's `calla.info.yml`. It needs three sections:

* Basic information such as the name and description.
* `regions`, which are the areas of the page that blocks can be put into. See [drupal.org documentation](https://www.drupal.org/docs/8/theming-drupal-8/defining-a-theme-with-an-infoyml-file#regions). At the most basic, the regions should be the same as used in the parent theme, Octavia.
* `libraries`, which allows us to substitute the parent theme's `.css` file with our customized version.

Here's an example. We'll cover the file mentioned in the `libraries` section (`dist/css/global.css`) later in the tutorial.

```yml
name: Calla
description: Customization of Drutopia's Bulma-based Octavia theme.
type: theme
base theme: octavia
core: 8.x

regions:
  header: 'Header'
  navbar_branding: 'Branding'
  header_search: 'Search'
  header_tabs: 'Tabs'
  primary_menu: 'Primary menu'
  secondary_menu: 'Secondary menu'
  highlighted: 'Highlighted'
  help: 'Help'
  content: 'Content'
  sidebar_first: 'Sidebar'
  tile_one: 'Tile 1'
  tile_two: 'Tile 2'
  tile_three: 'Tile 3'
  tile_four: 'Tile 4'
  tile_five: 'Tile 5'
  bottom: 'Bottom'
  footer: 'Footer'

libraries-override:
  bulma/global:
    css:
      base:
        /themes/contrib/octavia/dist/css/bulma.css: css/mystyles.css
```

### Set up theme development

We'll be writing files in SCSS, which needs to be compiled into CSS. In order to do this, we need some software.

The first step is to [install node.js](https://nodejs.org/en/download).

### Set up and use `node-sass`

Once you have `node.js` installed, follow the documentation on [customizing Bulma with `node-sass`](https://bulma.io/documentation/customize/with-node-sass), with some minor variations:

In [step 2](https://bulma.io/documentation/customize/with-node-sass/#2-install-the-dev-dependencies), we need to install an additional package. This is because Octavia uses [a Bulma extension to style timelines](https://wikiki.github.io/components/timeline).

```
npm install bulma-extensions --save-dev
```

In [step 5](https://bulma.io/documentation/customize/with-node-sass/#5-add-node-scripts-to-build-your-css), while you're editing `package.json` file to add the needed scripts, you can make some other changes like adding an author and setting the license. Here's an example of how it might look after edits:

```json
{
  "name": "calla",
  "version": "1.0.0",
  "description": "A subtheme of Octavia for use with the Drutopia Drupal distribution",
  "main": "sass/mystyles.scss",
  "scripts": {
    "css-build": "node-sass --omit-source-map-url sass/mystyles.scss css/mystyles.css",
    "css-watch": "npm run css-build -- --watch",
    "start": "npm run css-watch"
  },
  "author": "Some One",
  "license": "GPL",
  "devDependencies": {
    "bulma": "^0.7.2",
    "bulma-extensions": "^3.0.0",
    "node-sass": "^4.9.4",
  }
}
```

In [step 6](https://bulma.io/documentation/customize/with-node-sass/#6-add-your-own-bulma-styles), the example `mystyles.scss` file given in step 6 uses only specific Bulma elements, but we need the full deal. We also need to import the Bulma timeline. So delete the lines from here on down:

```
// Import only what you need from Bulma
```

Replace them with this:

```
// Add bulma and the timeline extension.
@import "../node_modules/bulma/bulma.sass";
@import '../node_modules/bulma-extensions/bulma-timeline/dist/css/bulma-timeline.sass';
```

When you have completed these steps, you should end up with a customized version of Bulma written to `css/mystyles.css`.

### Create config files

* Copy the [`config` folder from Octavia](https://gitlab.com/drutopia/octavia/tree/8.x-1.x/config).
* In all subdirectories of the copied `config` folder:
    * Rename file names to use the new theme name rather than `octavia`. In our example, we replace `octavia` with `calla` in all file names.
    * Within those files, replace any reference to `octavia` with your new theme name.

### Install your theme

* Your theme should now be ready to install.
