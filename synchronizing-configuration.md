# Synchronizing Configuration

Drutopia relies on a complex suite of modules that have been written to allow for shared configuration in Drupal 8.

Drutopia member Nedjo Rogers has written comprehensive technical details in an eight-part series. If you want to understand what is at play here this series comprises the following posts:

* [Managing Shared Configuration Part 1: Configuration Providers](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-1-configuration-providers)
* [Managing Shared Configuration Part 2: Configuration Snapshots](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-2-configuration-snapshots)
* [Managing Shared Configuration Part 3: Respecting Customizations](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-3-respecting-customizations)
* [Managing Shared Configuration Part 4: Configuration Alters](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-4-configuration-alters)
* [Managing Shared Configuration Part 5: Updating from Extensions](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-5-updating-extensions)
* [Managing Shared Configuration Part 6: Packaging Configuration with the Features Module](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-6-packaging-configuration-features-module)
* [Managing Shared Configuration Part 7: Core Configuration](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-7-core-configuration)
* [Managing Shared Configuration Part 8: Summary and Future Directions](http://chocolatelilyweb.ca/blog/managing-shared-configuration-part-8-summary-and-future-directions)
* Bonus post: [Drupal distributions, blocks, and subthemes](http://chocolatelilyweb.ca/blog/drupal-distributions-blocks-and-subthemes)

While the back-end is incredibly complex, the goal for site administrators is to provide a simple way for you to get new improvements from Drutopia and its features, while allowing you to keep any simple customizations you have made.

## Importing configuration updates:
* Navigate to the Distribution Update page (admin/config/development/configuration/distro).
* The help text at the top of the page provides clear guidance:  
    Any available configuration updates from installed modules or themes are displayed here.  

    If you're an advanced user, you may wish to use the checkboxes to select which modules and themes to run updates from. Running some updates while skipping others can lead to unexpected results and so should be done with caution.  

    By default, changes will be merged into the site's active configuration so as to retain any customizations you've made. For example, if you've edited the label of a field for which updates are available, that edit will be retained.  

    Advanced users may choose instead to reset configuration to the state currently provided by installed modules, themes, and the install profile.

* For most instances, you will merge in all the changes by clicking the import button.
* You will get any new changes to features, such as new fields or a new paragraph type, while customizations you made, such as changing the title of a view, will be maintained.
 While you have now added the newly available elements, changes you have made to your site’s configuration will not be overridden, such as a views title that you changed from Article to News.

## Advanced reset options:
* There are also two advanced modes to reset configuration to its default settings (that is, overriding any customizations you have made).
* The partial reset option resets only configuration for which there is a change available.
* The full reset option, resets all configuration on the site (including for example the site name and email address) to the default values as if the site were just being installed.
* If you want to use these advanced options (only recommended for developers), start by selecting the update mode, and clicking the “Change update mode” button.
* Once the page has reloaded, you can review the changes and then if desired, click the Import button.

## Importing configuration from a newly turned on feature:
* Importing configuration is also a tool to use when enabling a new Drutopia feature after your initial install. 
* For example, when you initially installed Drutopia you did not select the Campaign feature. Now you’d like to add it to your site. 
* Enable the feature from the modules page as usual. You’ll get the campaign content type, but certain optional configuration will not be supplied, for example the block of promoted campaigns for the home page. 
* You can easily pull in this block by importing the configuration as described above. 
* Your home page will now be updated to include that home page block (and its default content).


