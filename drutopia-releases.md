# Drutopia releases complete changelog

Each release listing includes the commits to all constituent Drutopia projects— the feature modules and themes that make up the Drutopia base distribution.  It is meant primarily as an aid for writing more human-friendly updates about what has changed in a release.

```{note}
   Adding information here is a manual process, so we may be missing releases.
```

## 1.0-rc1
### drutopia_action
[8730755](https://gitlab.com/drutopia/drutopia_action/commit/87307558d09a5ae9f33102c48cbca8fc4728b339) Merge branch '25-content-bottom' into '8.x-1.x' (Nedjo Rogers)  
[569787d](https://gitlab.com/drutopia/drutopia_action/commit/569787d7633c7fa2787d38b41930a6d34e903bfe) Issue #25: Pin action link to new card bottom region (Rosemary Mann)  
[eca46de](https://gitlab.com/drutopia/drutopia_action/commit/eca46de7593ae6d79b6f7c401c1ce08f04995c4f) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[a49016c](https://gitlab.com/drutopia/drutopia_action/commit/a49016c1fdf642fb6921c7002eecb72d423a22a9) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_article
[8149d3c](https://gitlab.com/drutopia/drutopia_article/commit/8149d3c1ff5fe91c0493e1dcb9da62896766a5cf) Merge branch '32-content-bottom' into '8.x-1.x' (Nedjo Rogers)  
[32ffdcf](https://gitlab.com/drutopia/drutopia_article/commit/32ffdcfe022d5cc24b75803aa872ad8485e1f661) Issue #32: Pin topic button to new card bottom region (Rosemary Mann)  
[2975883](https://gitlab.com/drutopia/drutopia_article/commit/2975883a0304128add0de90672bd066130dab960) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[193bb35](https://gitlab.com/drutopia/drutopia_article/commit/193bb35ca38bb1fda3f91847b6b32d61bd18c2cb) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_blog
[00fcd75](https://gitlab.com/drutopia/drutopia_blog/commit/00fcd75ec6a728fb158fa58f5ad9b08ef7fa2e99) Merge branch '25-content-bottom' into '8.x-1.x' (Nedjo Rogers)  
[952d46a](https://gitlab.com/drutopia/drutopia_blog/commit/952d46aad4663b69c2a348cb03329eef5bfd78e9) Issue #25: Pin topic button to new card bottom region (Rosemary Mann)  
[e1078a6](https://gitlab.com/drutopia/drutopia_blog/commit/e1078a65a584acf49e3573fef08245e926eee8a4) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[e41081d](https://gitlab.com/drutopia/drutopia_blog/commit/e41081d5767eebc3fe4dcf2a1df1f28eabbc7622) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_campaign
[d6dddc0](https://gitlab.com/drutopia/drutopia_campaign/commit/d6dddc006188e143ee3d41bea417cfef32b47ea4) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[6bd66f3](https://gitlab.com/drutopia/drutopia_campaign/commit/6bd66f307975f52b82032112922e4e19a19a25c3) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_comment
[ec79715](https://gitlab.com/drutopia/drutopia_comment/commit/ec797158e326d9f79d00f1be040abb6080b8d7dc) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[144ef0f](https://gitlab.com/drutopia/drutopia_comment/commit/144ef0f5034cf448bc90ce5b193445835f4ec9ae) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_core
[946361f](https://gitlab.com/drutopia/drutopia_core/commit/946361ff74eaa106b706aa084b3337527412089e) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[fb24333](https://gitlab.com/drutopia/drutopia_core/commit/fb24333b3b518222b5c9b5a42c9ac9559cc11cd2) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_event
[f3a2ba0](https://gitlab.com/drutopia/drutopia_event/commit/f3a2ba0e9660795cd1262501598b16662db82aa7) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[71f1970](https://gitlab.com/drutopia/drutopia_event/commit/71f197064ad4f77ed3b4609b798089de6a6a541f) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_fundraising
[fca7850](https://gitlab.com/drutopia/drutopia_fundraising/commit/fca7850cc16aa8279727b25b216879cf45a7efb1) Use new style of declaring dependencies (benjamin melançon)  
[e54576a](https://gitlab.com/drutopia/drutopia_fundraising/commit/e54576af783b00324233f895888b0cb979db48ab) Update minimum versions of dependencies (benjamin melançon)  
### drutopia
[2b28a9a](https://gitlab.com/drutopia/drutopia/commit/2b28a9a06f4ad396fc151177b0c42cd1c7445ff8) Merge branch '290-antispam' into '8.x-1.x' (Nedjo Rogers)  
[2c46372](https://gitlab.com/drutopia/drutopia/commit/2c46372af82884781102d8945f2b43d464e49f81) Issue #290: Add Antibot, Honeypot and Riddler modules to the install profile (Rosemary Mann)  
[3742b1d](https://gitlab.com/drutopia/drutopia/commit/3742b1dbdc14d9c85d16b2cc235abe74d9a4eb61) Issue #271: less radical approach to simplify composer.json file (Nedjo Rogers)  
[5a0b16b](https://gitlab.com/drutopia/drutopia/commit/5a0b16bcb5b670a3e8c0856fee1d395d6be5745b) Revert "Issue #271: simplify composer.json file" (Nedjo Rogers)  
[0f6e695](https://gitlab.com/drutopia/drutopia/commit/0f6e695665ba496093beb042cfdd8a09db9ca4e3) Issue #271: simplify composer.json file (Nedjo Rogers)  
[2384f3a](https://gitlab.com/drutopia/drutopia/commit/2384f3a67a7f12277d577276ef817b9bbaae3161) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[d969995](https://gitlab.com/drutopia/drutopia/commit/d9699959958ba025608b79ccfe0152140687d140) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_group
[7df38df](https://gitlab.com/drutopia/drutopia_group/commit/7df38df5cc8574b589934eac4f2f598c47ed91fb) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[afbe6f6](https://gitlab.com/drutopia/drutopia_group/commit/afbe6f67d726e8a9e5889f222262b97ca94ec5fa) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_landing_page
[c8692c0](https://gitlab.com/drutopia/drutopia_landing_page/commit/c8692c0d75edf67e97f3b3a97926ed929ad17cc6) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[a7142c5](https://gitlab.com/drutopia/drutopia_landing_page/commit/a7142c52d886605c3ac2044d604f513e1158c86d) Feature regeneration for cleanup only (Rosemary Mann)  
[b6da33d](https://gitlab.com/drutopia/drutopia_landing_page/commit/b6da33d2e8d09e22c05aafdf58ee702388baf877) Stop inflicting the words 'Body paragraph' on people using screen readers (Benjamin Melançon)  
[9e2e102](https://gitlab.com/drutopia/drutopia_landing_page/commit/9e2e102724e74afe175c227ac91eea499a2fe87e) Revert "Add missing required module to composer dependencies" (Benjamin Melançon)  
[9d0a56d](https://gitlab.com/drutopia/drutopia_landing_page/commit/9d0a56d339e78007b13157cfa6e23836005c6a83) Add missing required module to composer dependencies (Benjamin Melançon)  
### drutopia_page
[3092317](https://gitlab.com/drutopia/drutopia_page/commit/309231710ca93b0f47ff227b79c68021d07d1705) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[fd57372](https://gitlab.com/drutopia/drutopia_page/commit/fd573725688f0c815ebcc3decd6ec935092a19bc) Feature regeneration for cleanup only (Rosemary Mann)  
[2f7211d](https://gitlab.com/drutopia/drutopia_page/commit/2f7211d020800a68ae0a96832ecb3f0c13f00fa4) Simplify terminology (benjamin melançon)  
### drutopia_people
[b92bc71](https://gitlab.com/drutopia/drutopia_people/commit/b92bc712e6b78d6ebbc5d680c30dac10448d328c) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[15a8f47](https://gitlab.com/drutopia/drutopia_people/commit/15a8f472865c8ae16ebfe327887a55559a11d295) Feature regeneration for cleanup only (Rosemary Mann)  
[62f88fe](https://gitlab.com/drutopia/drutopia_people/commit/62f88fe7a3041838cfa1a0910154aaac7baec35e) Make content type label singular like all the others (benjamin melançon)  
### drutopia_related_content
[3cb61e5](https://gitlab.com/drutopia/drutopia_related_content/commit/3cb61e54309a34d9faba34948a7be88b2a7d5e60) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[001c08b](https://gitlab.com/drutopia/drutopia_related_content/commit/001c08bd00046ed7cef4c1a988ed034f44c414b9) Feature regeneration for cleanup only (Rosemary Mann)  
[01987ce](https://gitlab.com/drutopia/drutopia_related_content/commit/01987ce6bb106cc52617b5442e005f0528faca09) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_resource
[a158a8a](https://gitlab.com/drutopia/drutopia_resource/commit/a158a8a9c1ab49aa12e6177098d8383d6dc4e48e) Merge branch '13-content-bottom' into '8.x-1.x' (Nedjo Rogers)  
[ce5d24d](https://gitlab.com/drutopia/drutopia_resource/commit/ce5d24d9e225cc5aa678d8197986a12fe69d030e) Issue #13: Pin topics button, resource link and resource file to new card bottom region (Rosemary Mann)  
[6ceada7](https://gitlab.com/drutopia/drutopia_resource/commit/6ceada7a72dc361ac86a4c6dbb834782fe2ecadc) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[d1184f2](https://gitlab.com/drutopia/drutopia_resource/commit/d1184f2e13d68a4bbdc8ce66f9e3d13ac1f851b2) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_search
[57f19d4](https://gitlab.com/drutopia/drutopia_search/commit/57f19d4befbd9e38ad5743f0a64e5fef4e1b24be) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[40cbd09](https://gitlab.com/drutopia/drutopia_search/commit/40cbd0981a88e67b6bfb73d454b74e1997e6f086) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_seo
[885b3c4](https://gitlab.com/drutopia/drutopia_seo/commit/885b3c4b08f00a6789e94537ef4cbf519ea247d3) Revert "Add Redirect 404 module for fixing file not found errors" (benjamin melançon)  
[79baff5](https://gitlab.com/drutopia/drutopia_seo/commit/79baff52b2891cdbb8c4d88d2bbcefff9017e76a) Enable Redirect 404 module for fixing file not found errors (benjamin melançon)  
[a470649](https://gitlab.com/drutopia/drutopia_seo/commit/a47064983815ed42734eaf98f139fd2388072581) Add Redirect 404 module for fixing file not found errors (benjamin melançon)  
### drutopia_site
[9606e2a](https://gitlab.com/drutopia/drutopia_site/commit/9606e2ab90311aeb41153c729e2a67bb01259991) admin_links_access_filter is now a submodule of admin toolbar, admin_toolbar_links_access_filter (benjamin melançon)  
[6da8667](https://gitlab.com/drutopia/drutopia_site/commit/6da8667f2fba3456522723af052ddd36ef34300c) Merge branch '15-admin-links-access' into '8.x-1.x' (Rosemary Mann)  
[a8b3004](https://gitlab.com/drutopia/drutopia_site/commit/a8b3004e77dbdfff6d6872334eaf73f134aac3f6) Issue #15: Replace obsolete module with Admin Toolbar Links Access Filter (Nedjo Rogers)  
[dcdf096](https://gitlab.com/drutopia/drutopia_site/commit/dcdf096c5150a0e09087b64f63aadc461d1d32c2) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[70520ff](https://gitlab.com/drutopia/drutopia_site/commit/70520ffbe695687b2e5d90ba2b7121b04912fb8d) Feature regeneration for cleanup only (Rosemary Mann)  
[9c39d72](https://gitlab.com/drutopia/drutopia_site/commit/9c39d723e1d58cf2814957fdf59f6d8533df2dbc) Give Managers ability to assign manager & editor roles (benjamin melançon)  
[fc77dae](https://gitlab.com/drutopia/drutopia_site/commit/fc77daed196a3b6973ca806149033b82758db587) Enable role delegation by default (benjamin melançon)  
[8e9897e](https://gitlab.com/drutopia/drutopia_site/commit/8e9897e21d04283a523ac978e7adda963a3390fc) Add role delegation module so site managers have a permission to grant roles (benjamin melançon)  
[7915932](https://gitlab.com/drutopia/drutopia_site/commit/7915932f9246f8952ccb437f3063df31ba8a032a) Add autosave form as requirement & enable (benjamin melançon)  
[c98f348](https://gitlab.com/drutopia/drutopia_site/commit/c98f348c15869923e890949c18d2fd3542dbbd4a) Enable site managers to edit the main menu > > Ref: > https://gitlab.com/find-it-program-locator/findit/issues/279 > drutopia/drutopia#252 > drutopia/drutopia_page#14 (benjamin melançon)  
[1e483a8](https://gitlab.com/drutopia/drutopia_site/commit/1e483a8828d641a8f5cb41d739d5e7c7a8be7612) Enable menu admin per menu and menu UI modules (benjamin melançon)  
[5048f90](https://gitlab.com/drutopia/drutopia_site/commit/5048f90bd70fd4735b54e8d918303373ea68ffe3) Add menu admin per menu to module to composer requirements > > Ref: > https://gitlab.com/find-it-program-locator/findit/issues/279 > drutopia/drutopia#252 > drutopia/drutopia_page#14 (benjamin melançon)  
[3ac94a8](https://gitlab.com/drutopia/drutopia_site/commit/3ac94a86a4e3823a5fdf9cb5005b0ab63162e40e) Upgrade to next major version of admin toolbar (benjamin melançon)  
[aa081e0](https://gitlab.com/drutopia/drutopia_site/commit/aa081e0b9f8239a9c1c802f3dad64a87bfc1cf88) Add dependency on linebreaks module (benjamin melançon)  
[ebd1d60](https://gitlab.com/drutopia/drutopia_site/commit/ebd1d606d8d711275bfe4014c700b7baae2bbb57) Add WYSYWIG Linebreaks module and alphabetize requirements (benjamin melançon)  
[6e08ef9](https://gitlab.com/drutopia/drutopia_site/commit/6e08ef9707d7f4a689a3eeb02adebaa632bf5fa3) Merge branch '9-fix-wysiwyg-heights' into '8.x-1.x' (Clayton Dewey)  
[337e46e](https://gitlab.com/drutopia/drutopia_site/commit/337e46e1b1a4ffc958c19cbf8e1833896bdf861f) Enable ckeditor height module (Benjamin Melançon)  
[ace6cea](https://gitlab.com/drutopia/drutopia_site/commit/ace6cea69b535fd2d78098fe43ee9d5bfef0e0bd) Require contrib CKEditor height module (Benjamin Melançon)  
### drutopia_social
### drutopia_storyline
[59e9099](https://gitlab.com/drutopia/drutopia_storyline/commit/59e90997364be336d90b1db2d6099a9ed58dd9fc) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[9d41e1c](https://gitlab.com/drutopia/drutopia_storyline/commit/9d41e1ccf1f329c86a723f520e207c5fddd29371) Feature regeneration for cleanup only (Rosemary Mann)  
### drutopia_user
[9363ae8](https://gitlab.com/drutopia/drutopia_user/commit/9363ae8d3219e90749cd5cea9b635a330dcb4751) Merge branch 'cleanup' into '8.x-1.x' (Nedjo Rogers)  
[7e9bc9a](https://gitlab.com/drutopia/drutopia_user/commit/7e9bc9a1e52646ba322dc3bda0a0a9ca454ab713) Feature regeneration for cleanup only (Rosemary Mann)  
### octavia
[c5c0b8c](https://gitlab.com/drutopia/octavia/commit/c5c0b8cd854da1173750edc3665a9afab8847cba) Merge branch '88-centred-site-slogan' into '8.x-1.x' (Rosemary Mann)  
[efe58d5](https://gitlab.com/drutopia/octavia/commit/efe58d5d7d592ef06bfead72447de309e64536c6) Issue #88: center site slogan (Nedjo Rogers)  
[cfaf936](https://gitlab.com/drutopia/octavia/commit/cfaf93643ca1f18be281b90e8f9ab91d32a026a0) Issue #86: adjust colours and page title in Radish skin (Rosemary Mann)  
[07cad6a](https://gitlab.com/drutopia/octavia/commit/07cad6a3c6db3c8f831ec870520d81c7f11461e3) Merge branch '80-site-slogan-padding' into '8.x-1.x' (Rosemary Mann)  
[ffc2ff8](https://gitlab.com/drutopia/octavia/commit/ffc2ff81e745b24e8f071d5989cc17230d278d05) Issue #80: fix site slogan padding (Nedjo Rogers)  
[789ee02](https://gitlab.com/drutopia/octavia/commit/789ee02fcfe3d4b86f765e66af4d8200c8133ad4) Merge branch '86-radish' into '8.x-1.x' (Nedjo Rogers)  
[648820c](https://gitlab.com/drutopia/octavia/commit/648820ccfefdb9de7d9d8ba08d08216079bc1334) Issue #86: Initial commit of skin Radish (Rosemary Mann)  
[6353c7e](https://gitlab.com/drutopia/octavia/commit/6353c7ee91c24e065b67d6e87931b7fbd789184e) Merge branch '74-action-link' into '8.x-1.x' (Nedjo Rogers)  
[70608f0](https://gitlab.com/drutopia/octavia/commit/70608f054988a192ec1137043b957abdbfdfaad9) Issue #74: Add size for mobile action link (Rosemary Mann)  
[b44a8d7](https://gitlab.com/drutopia/octavia/commit/b44a8d73b4a7d307eb439536292b0084502ca5ee) Merge branch '85-variables-merge' into '8.x-1.x' (Rosemary Mann)  
[d3d9d7f](https://gitlab.com/drutopia/octavia/commit/d3d9d7fcc0b3adf38c70b75ea92f4a22219a888d) Issue #85: merge into existing variables rather than resetting (Nedjo Rogers)  
[353ca62](https://gitlab.com/drutopia/octavia/commit/353ca62af3dd84ce280fbf0b967e089adae68e82) Merge branch '85-node-title' into '8.x-1.x' (Rosemary Mann)  
[8e0a60c](https://gitlab.com/drutopia/octavia/commit/8e0a60ceb3d0073213bac3c9eea9023f56f7da2d) Issue #85: fix title element on node title field (Nedjo Rogers)  
[f02b4a2](https://gitlab.com/drutopia/octavia/commit/f02b4a23a8e9867adeda651c33b6e54c08e292e4) Merge branch '84-more-link-style' into '8.x-1.x' (Rosemary Mann)  
[e1115bd](https://gitlab.com/drutopia/octavia/commit/e1115bd0e5d50ed9e06152a1c5c7a73a2ca6878c) Merge branch '82-content-bottom' into '8.x-1.x' (Rosemary Mann)  
[4d08baa](https://gitlab.com/drutopia/octavia/commit/4d08baa254a5a58742f0927ee39647cbdcfbace8) Issue #82: enable pinning field items to bottom of card (Nedjo Rogers)  
[08ac4d4](https://gitlab.com/drutopia/octavia/commit/08ac4d4cdbc3cb22a2615b0fe4311658b2ee767a) Issue #84: style more link as a button with a plus icon (Nedjo Rogers)  
[8175917](https://gitlab.com/drutopia/octavia/commit/817591746cf65735cd8eb338276a936420bedb72) Issue #83: Set width on content_bottom region block columns (Rosemary Mann)  
[7ab9a7a](https://gitlab.com/drutopia/octavia/commit/7ab9a7a2cf1067bef08e094710f7ecff333576a8) Issue #80: calling twig parent() within a block not working so fork navbar block contents from bulma base theme (Rosemary Mann)  
[d0e6679](https://gitlab.com/drutopia/octavia/commit/d0e66792a753b626bb0c18620513678a885d1113) Merge branch '81-equal-height' into '8.x-1.x' (Nedjo Rogers)  
[f169140](https://gitlab.com/drutopia/octavia/commit/f169140add9bcd3e7ac490c5f2cc0071b7f8020b) Issue #81: equal height columns (Rosemary Mann)  
[f311904](https://gitlab.com/drutopia/octavia/commit/f3119042390244ecf55b56411c68de5638da044b) Issue #80: further tweaks for mobile (Rosemary Mann)  
[5a2d696](https://gitlab.com/drutopia/octavia/commit/5a2d696d2ecc84054babfd93dd28d3e5db6d045a) Commit compiled CSS (benjamin melançon)  
[6336c5e](https://gitlab.com/drutopia/octavia/commit/6336c5e19d80ae60ff752c506a772b5fbc7f5b6f) Ensure empty indicator marks hidden despite tag styling; position visible mark to left (benjamin melançon)  
[b57a029](https://gitlab.com/drutopia/octavia/commit/b57a0291f9ae01e581852d1bd04989f48df32d02) Add class for styling, move created, date-as-permalink up to left column (benjamin melançon)  
[b7f0179](https://gitlab.com/drutopia/octavia/commit/b7f0179f5152c4eec5cd78133884c2743393662c) Add permalink URL variable to allow remix of date as permalink (benjamin melançon)  
[23aad18](https://gitlab.com/drutopia/octavia/commit/23aad1879b873a9145fe2ab18ec3f8e087ec0fc0) Move author / created to left column (drop submitted by text) (benjamin melançon)  
[1120253](https://gitlab.com/drutopia/octavia/commit/1120253bb6029324d9f16ed03b7e8f17ca6beae5) Override comment template to remove title (benjamin melançon)  
[bee154a](https://gitlab.com/drutopia/octavia/commit/bee154a8d638e2681e493e6e0151a863e0c342c6) Merge branch '80-mobile-margin' into '8.x-1.x' (Nedjo Rogers)  
[f24ce4a](https://gitlab.com/drutopia/octavia/commit/f24ce4aee11ececfc837c1a0b880bb755136e32b) Adjust padding on various elements for specific breakpoints and hide footer menus at mobile (Rosemary Mann)  
[f657e4e](https://gitlab.com/drutopia/octavia/commit/f657e4e78e67562b71ffb084c265f492bd21a410) Merge branch '79-bulma-helpers' into '8.x-1.x' (Nedjo Rogers)  
[98797d1](https://gitlab.com/drutopia/octavia/commit/98797d1bc0ef9e36b184787f17edf1f6f69ff028) Issue #79: add bulma-helpers library (Rosemary Mann)  
[688378b](https://gitlab.com/drutopia/octavia/commit/688378bbb3c39f3383c09e24324482dea6217d3d) Merge branch 'bulma-update' into '8.x-1.x' (Nedjo Rogers)  
[b4d21c1](https://gitlab.com/drutopia/octavia/commit/b4d21c1daf9fe9a7e428b0b0531ddccd77fbd38c) Update to Bulma 0.8.0 (Rosemary Mann)  
[af37169](https://gitlab.com/drutopia/octavia/commit/af371696645d7a684f9fa7a78f4ea51de38bac70) Issue #78: tweak Cool skin styling (Nedjo Rogers)  
[39c662b](https://gitlab.com/drutopia/octavia/commit/39c662b460e438bd83b43aab4bac27e2e886fcaa) Issue #78: add Vollkorn font to Cool skin (Rosemary Mann)  
[dd49a25](https://gitlab.com/drutopia/octavia/commit/dd49a2545a8b58df726c7cf38515f03182748068) Merge branch '78-cool' into '8.x-1.x' (Nedjo Rogers)  
[66b83cf](https://gitlab.com/drutopia/octavia/commit/66b83cf9e747279fa8c3888db93e506a3697b61a) Issue #78: New skin Cool (Rosemary Mann)  
[d4531d4](https://gitlab.com/drutopia/octavia/commit/d4531d4b785b95531cd350d1c74132b6c050657e) Merge branch '77-logo' into '8.x-1.x' (Nedjo Rogers)  
[123750c](https://gitlab.com/drutopia/octavia/commit/123750c0555463ca42551dd61d8dfd3347f6bd38) Issue #77: Fix for logo (Rosemary Mann) 
## 1.0-beta2
### drutopia_action
[78fe14e](https://gitlab.com/drutopia/drutopia_action/commit/78fe14ea52789ebc7c664e150ac42b44d2f1ceff) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[3629ed4](https://gitlab.com/drutopia/drutopia_action/commit/3629ed4072169e1fff1372acb1dfd16cda588e75) Update parent menu item (Rosemary Mann)  
[1a3cd39](https://gitlab.com/drutopia/drutopia_action/commit/1a3cd3956c8ccbac51ec9a756fee4e954be72ea2) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[1feea18](https://gitlab.com/drutopia/drutopia_action/commit/1feea18d4bc8c352901b092770711d277f9507d5) Update to use focal point widget (Rosemary Mann)  
[fb6ff45](https://gitlab.com/drutopia/drutopia_action/commit/fb6ff45208ab7ec33d48dcb58bad438cc057ab0f) Merge branch '24-display' into '8.x-1.x' (Nedjo Rogers)  
[feffa4b](https://gitlab.com/drutopia/drutopia_action/commit/feffa4bbf1ab9366a12bb11ad80e8c60b2e43c95) Issue #24: Disable css (Rosemary Mann)  
### drutopia_article
[192c7db](https://gitlab.com/drutopia/drutopia_article/commit/192c7db42ce5bc4ad0c936fca22a6b2b8e1909e8) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[5118b7b](https://gitlab.com/drutopia/drutopia_article/commit/5118b7b0247be8d0f066b1e69431dc4bdf499818) Update parent menu item and title (Rosemary Mann)  
[a5f92a1](https://gitlab.com/drutopia/drutopia_article/commit/a5f92a19f7740efdd2bdffef500083b251439615) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[76194c5](https://gitlab.com/drutopia/drutopia_article/commit/76194c5af321a8607d42f93e8dab955a80a23e85) Update to use focal point widget (Rosemary Mann)  
[484122d](https://gitlab.com/drutopia/drutopia_article/commit/484122dce4b417c9647a90f3b1f12ac9b7f160e6) Merge branch '28-search' into '8.x-1.x' (Nedjo Rogers)  
[f17a54d](https://gitlab.com/drutopia/drutopia_article/commit/f17a54df7a63306b23525e0f53a6d3f105fc14a6) Issue #28: Configure search index view mode (Rosemary Mann)  
[5324e62](https://gitlab.com/drutopia/drutopia_article/commit/5324e62a8d2e5327aa473c5e7ab6d7ac7f70fdaa) Merge branch '31-display' into '8.x-1.x' (Nedjo Rogers)  
[5a13092](https://gitlab.com/drutopia/drutopia_article/commit/5a130921b82f0823fb7e19adff33250efefe31ce) Issue #31: Disable css (Rosemary Mann)  
### drutopia_blog
[e682400](https://gitlab.com/drutopia/drutopia_blog/commit/e682400080fce5a5d63ce47d715e69ec5bb1b7e1) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[d4b2977](https://gitlab.com/drutopia/drutopia_blog/commit/d4b29773fa0ddfd682f755a523958eee97dbc995) Update parent menu item (Rosemary Mann)  
[52c6361](https://gitlab.com/drutopia/drutopia_blog/commit/52c636160f2fd35f9938ed07a574610edbdf8263) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[e01b80d](https://gitlab.com/drutopia/drutopia_blog/commit/e01b80dde0ce99470a366e44a3970c6483be8e5c) Update to use focal point widget (Rosemary Mann)  
[d05cb48](https://gitlab.com/drutopia/drutopia_blog/commit/d05cb48a249556457d9cae4cc75dc38eaf653bd2) Merge branch '23-display' into '8.x-1.x' (Nedjo Rogers)  
[171095d](https://gitlab.com/drutopia/drutopia_blog/commit/171095dea18735d0dcd82b33c666ef8ca8a17e7c) Issue #23: Disable css (Rosemary Mann)  
### drutopia_campaign
[42c0e0c](https://gitlab.com/drutopia/drutopia_campaign/commit/42c0e0cdd920e8b1aaff5a82a48599c1c828dabc) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[8806197](https://gitlab.com/drutopia/drutopia_campaign/commit/8806197a2c6bcfa2a56522eceacb359bc96ea5a2) Update parent menu item (Rosemary Mann)  
[3c31260](https://gitlab.com/drutopia/drutopia_campaign/commit/3c31260d8fd778a0913c62496d9451f87c98b2fe) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[6b17b2e](https://gitlab.com/drutopia/drutopia_campaign/commit/6b17b2e13a52a9f03535d9f999dfa2c10ce8dfc4) Update to use focal point widget (Rosemary Mann)  
[62d8fa0](https://gitlab.com/drutopia/drutopia_campaign/commit/62d8fa014fbb433f3a97476303fffca11dabc7c5) Merge branch '10-display' into '8.x-1.x' (Nedjo Rogers)  
[8323cdb](https://gitlab.com/drutopia/drutopia_campaign/commit/8323cdb443385d9aff3258e432d2afaf36509c9b) Configure teaser as two column and disable css (Rosemary Mann)  
### drutopia_core
[234352c](https://gitlab.com/drutopia/drutopia_core/commit/234352cd66aa256b66730d5f53ee992889b15eef) Merge branch '31-dependency' into '8.x-1.x' (Nedjo Rogers)  
[669ddd7](https://gitlab.com/drutopia/drutopia_core/commit/669ddd759a11bfadf8888daed435dd50cb3ab81a) Issue #31: add dependency on Default Content module (Rosemary Mann)  
[1760f9f](https://gitlab.com/drutopia/drutopia_core/commit/1760f9f917aed8a069cb0d63eeab641451463001) Merge branch '31-menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[ae3180b](https://gitlab.com/drutopia/drutopia_core/commit/ae3180b11385fd3db29630f5a4ef2d1a29e371a3) Issue #31: Add top level menu items as default content (Rosemary Mann)  
[059952b](https://gitlab.com/drutopia/drutopia_core/commit/059952ba1f4e732f6657066efc10848df142b792) Merge branch '33-responsive' into '8.x-1.x' (Nedjo Rogers)  
[fb9bb65](https://gitlab.com/drutopia/drutopia_core/commit/fb9bb65e80c6888e8565f50d63a2f7dbff836a87) Issue #33: Add new image styles and responsive image styles (Rosemary Mann)  
[917bf1a](https://gitlab.com/drutopia/drutopia_core/commit/917bf1ae2185fba308498be054c41acf17c94532) Merge branch '32-crop' into '8.x-1.x' (Nedjo Rogers)  
[3b8a99d](https://gitlab.com/drutopia/drutopia_core/commit/3b8a99de39d20e2c8be3bdc326b927c437de5e37) Tweak ordering of modules in update (Rosemary Mann)  
[2b4463d](https://gitlab.com/drutopia/drutopia_core/commit/2b4463ddf474ffffdf7cdb7ccbb6986e3e9c9c4d) Issue #32: Add Focal Point and Crop API modules (Rosemary Mann)  
[41380cd](https://gitlab.com/drutopia/drutopia_core/commit/41380cd5fada696f2eb19fff5dfb04f491bfd768) Merge branch '30-empty-facets' into '8.x-1.x' (Rosemary Mann)  
[6680f54](https://gitlab.com/drutopia/drutopia_core/commit/6680f54d5f569ce4dcee4158f1c08b480b9512b0) Issue #30: add patch on facets module to hide empty facet blocks (Nedjo Rogers)  
### drutopia_event
[cd2cd9f](https://gitlab.com/drutopia/drutopia_event/commit/cd2cd9f34a4ee8273fce882ab3612f7062bfcc54) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[d32e1d8](https://gitlab.com/drutopia/drutopia_event/commit/d32e1d8fe69cc7904300aa03cef9294d20f6ec7e) Update parent menu item (Rosemary Mann)  
[690c996](https://gitlab.com/drutopia/drutopia_event/commit/690c996b31356474b5445dcf697862eac10c919a) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[ff7c874](https://gitlab.com/drutopia/drutopia_event/commit/ff7c87425b3ec241da5da2e8578949c1b4a3c5d6) Update to use focal point widget (Rosemary Mann)  
### drutopia_fundraising
[fca7850](https://gitlab.com/drutopia/drutopia_fundraising/commit/fca7850cc16aa8279727b25b216879cf45a7efb1) Use new style of declaring dependencies (benjamin melançon)  
[e54576a](https://gitlab.com/drutopia/drutopia_fundraising/commit/e54576af783b00324233f895888b0cb979db48ab) Update minimum versions of dependencies (benjamin melançon)  
### drutopia
[6d08e07](https://gitlab.com/drutopia/drutopia/commit/6d08e0712cc63f03ea884b367da2ae745e34800d) Merge branch '265-upgradee-to-core-8-7' into '8.x-1.x' (Rosemary Mann)  
[1f38ad9](https://gitlab.com/drutopia/drutopia/commit/1f38ad978bd326f932f3b0068310eb53a97e794a) Issue #265: update to Drupal core 8.7.2 (Nedjo Rogers)  
[7ddb6c9](https://gitlab.com/drutopia/drutopia/commit/7ddb6c93ee35df89b1a83a64df4e1bc7ebaf3304) Merge branch '266-module-versions' into '8.x-1.x' (Nedjo Rogers)  
[a8225d7](https://gitlab.com/drutopia/drutopia/commit/a8225d77461688f9e150ab81fffaef80baa3c764) Issue #266: Update module versions as housekeeping (Rosemary Mann)  
[63444f7](https://gitlab.com/drutopia/drutopia/commit/63444f7a454a61ca8dab5a687500afa5b90aa0ac) Merge branch 'move-search-form' into '8.x-1.x' (Rosemary Mann)  
[40a4570](https://gitlab.com/drutopia/drutopia/commit/40a4570a0a842694c4048e137af9daa8d64c0e79) Move search form block to content region (Nedjo Rogers)  
[45e1c1b](https://gitlab.com/drutopia/drutopia/commit/45e1c1bccfa4322dd01bc5531eaafed8d4f5fe28) Merge branch '264-social-media' into '8.x-1.x' (Nedjo Rogers)  
[df31635](https://gitlab.com/drutopia/drutopia/commit/df3163578fc61508067302e618df924c994a7f54) Issue #264: Move social media block (Rosemary Mann)  
[fbf526b](https://gitlab.com/drutopia/drutopia/commit/fbf526b32e9872e1103ccea996b6b56de7eb163f) Merge branch '264-optional' into '8.x-1.x' (Nedjo Rogers)  
[5597523](https://gitlab.com/drutopia/drutopia/commit/5597523ff35fca86038f389fe2c0a2175765b5c5) Issue #264: Move new footer blocks to optional (Rosemary Mann)  
[b333bb6](https://gitlab.com/drutopia/drutopia/commit/b333bb6fd44e471e4b89445124c1198ff4a3ad4a) Merge branch '264-footer-blocks' into '8.x-1.x' (Nedjo Rogers)  
[e5aeefb](https://gitlab.com/drutopia/drutopia/commit/e5aeefb62936230111506072e53fff551fcea8d1) Issue #264: Add and update footer blocks (Rosemary Mann)  
[7453ce9](https://gitlab.com/drutopia/drutopia/commit/7453ce9e1f61e55f047c97cdd442111befdec12b) Merge branch 'bundle' into '8.x-1.x' (Nedjo Rogers)  
[f9e6103](https://gitlab.com/drutopia/drutopia/commit/f9e61030fc8c34f86cf61a2d7da6d1ce7349da50) Merge branch 'author-content' into '8.x-1.x' (Nedjo Rogers)  
[907e2c2](https://gitlab.com/drutopia/drutopia/commit/907e2c2903d71c800312ef38c76a89032c252272) Add content by author block to optional folder (Rosemary Mann)  
[eddb8fe](https://gitlab.com/drutopia/drutopia/commit/eddb8feb6e5c528ef3dd025d538d23233c05f5c9) Remove Drutopia bundle (Rosemary Mann)  
[efdf757](https://gitlab.com/drutopia/drutopia/commit/efdf757b504d5a5dc76d920f4bcf848873c3e450) Merge branch 'search-facets' into '8.x-1.x' (Nedjo Rogers)  
[863326e](https://gitlab.com/drutopia/drutopia/commit/863326e1cde5fcb24b9776a0b5be9d19bf4f2c43) Merge branch '262-remove-global-search-block' into '8.x-1.x' (Rosemary Mann)  
[6079327](https://gitlab.com/drutopia/drutopia/commit/60793271dbfaade3a1dd83df455e52c0c5169e8c) Add searcg facet blocks to optional folder (Rosemary Mann)  
[8cadf44](https://gitlab.com/drutopia/drutopia/commit/8cadf44d2752603fa14dff8375bcf1edd4b0b7c3) Issue #262: remove global search block (Nedjo Rogers)  
[cb2ff1f](https://gitlab.com/drutopia/drutopia/commit/cb2ff1f05f982431ab4a2145c26181e828958ee9) Merge branch '225-home-page-setting' into '8.x-1.x' (Rosemary Mann)  
[b114678](https://gitlab.com/drutopia/drutopia/commit/b114678fa8f934b08a95c59bbfb5a83617c58d59) Issue #225: fix node path (Nedjo Rogers)  
[1a451cc](https://gitlab.com/drutopia/drutopia/commit/1a451cc671a3a85beb9aa9e9a695fd3d385d087b) Issue #225: set the site front page to a system path rather an alias (Nedjo Rogers)  
[1f24c61](https://gitlab.com/drutopia/drutopia/commit/1f24c618bfcf820d78816ef7607868536f77bf48) Use empty array for paragraph behavior settings that won't blow up default content (benjamin melançon)  
### drutopia_group
[a92a7ad](https://gitlab.com/drutopia/drutopia_group/commit/a92a7ad3490d85f9c583443d0e2ac20056fce428) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[f5ffc62](https://gitlab.com/drutopia/drutopia_group/commit/f5ffc62314040aefde862dc0437203cdacc88867) Update parent menu item (Rosemary Mann)  
### drutopia_page
[fc39d22](https://gitlab.com/drutopia/drutopia_page/commit/fc39d22a0945dadb34521dc4602b74cb34b94493) Merge branch '15-default-content' into '8.x-1.x' (Nedjo Rogers)  
[0901e20](https://gitlab.com/drutopia/drutopia_page/commit/0901e2001690beeeda52de5ac6af8e692888479c) Issue #15: Update default content for privacy policy (Rosemary Mann)  
[a7709a8](https://gitlab.com/drutopia/drutopia_page/commit/a7709a8a6396f288132f5d0d73a9c151f12721f5) Merge branch '15-privacy' into '8.x-1.x' (Nedjo Rogers)  
[404ac35](https://gitlab.com/drutopia/drutopia_page/commit/404ac35f01a932a7959bfffec5cf8af5b7298787) Add default content for privacy policy (Rosemary Mann)  
[d648c0d](https://gitlab.com/drutopia/drutopia_page/commit/d648c0dc532a1113d6b58772042af8f3b6ed2548) Merge branch '31-about' into '8.x-1.x' (Nedjo Rogers)  
[c4e53a7](https://gitlab.com/drutopia/drutopia_page/commit/c4e53a74b69b7005c0bebf4c2009ea7999b744b2) Update default content for about menu link (Rosemary Mann)  
### drutopia_people
[c9c0df9](https://gitlab.com/drutopia/drutopia_people/commit/c9c0df914c8ffbc421773c586783344a7f4051f1) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[a11c13e](https://gitlab.com/drutopia/drutopia_people/commit/a11c13ea3004d00d61e068965e2e032206a9103e) Add menu item and parent (Rosemary Mann)  
[d41d87b](https://gitlab.com/drutopia/drutopia_people/commit/d41d87b9ec58cc27a17a0a554b5fc00a57ffc617) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[30cc34d](https://gitlab.com/drutopia/drutopia_people/commit/30cc34d61d66db012686e27e3a187789910b72ee) Update form to use focal point wdiget and update styles to use new tall and short responsive image styles (Rosemary Mann)  
[108cc42](https://gitlab.com/drutopia/drutopia_people/commit/108cc42028261c19319e7703ad09f4803c3ceda9) Merge branch '11-tweak' into '8.x-1.x' (Nedjo Rogers)  
[221d03f](https://gitlab.com/drutopia/drutopia_people/commit/221d03f0236915193d3e3b74633d202ccdecf542)  Issue #11: tweak view to hide if empty (Rosemary Mann)  
[1524e06](https://gitlab.com/drutopia/drutopia_people/commit/1524e0690fc9b27794b4ad77ccbb4024e3a0bcce) Merge branch '11-author' into '8.x-1.x' (Nedjo Rogers)  
[50f4b43](https://gitlab.com/drutopia/drutopia_people/commit/50f4b432b7054aad9ad8822ffa62cca8d41259c4) Issue #11: Add content by author view (Rosemary Mann)  
### drutopia_related_content
[72dcf2d](https://gitlab.com/drutopia/drutopia_related_content/commit/72dcf2d1d313a80fd2d65171f6ee3800d2e05fc5) Merge branch '2-people' into '8.x-1.x' (Nedjo Rogers)  
[6b4468a](https://gitlab.com/drutopia/drutopia_related_content/commit/6b4468ae20dadaefa52476580a6d544a362b34a0) Issue #2: Remove people and landing page content types from related content (Rosemary Mann)  
### drutopia_resource
[564ce4f](https://gitlab.com/drutopia/drutopia_resource/commit/564ce4f0f3cde680105e6e24252a3012dee935df) Merge branch 'menu-hierarchy' into '8.x-1.x' (Nedjo Rogers)  
[7a6cc88](https://gitlab.com/drutopia/drutopia_resource/commit/7a6cc8810e198f5e2b502162f95a8f567918a686) Update menu parent item (Rosemary Mann)  
[cf17528](https://gitlab.com/drutopia/drutopia_resource/commit/cf175282eb0545eabfe9b8f9aa760fd2f3b9738b) Merge branch 'focalpoint-widget' into '8.x-1.x' (Nedjo Rogers)  
[96a0dc1](https://gitlab.com/drutopia/drutopia_resource/commit/96a0dc1f18c1899792f5c3b22a2935402941b97f) Update to use focal point widget and update image style used for video field display (Rosemary Mann)  
### drutopia_search
[b0307b7](https://gitlab.com/drutopia/drutopia_search/commit/b0307b7f154ef1e5d19940bfb0d479715c8ba4cf) Merge branch '14-caching' into '8.x-1.x' (Nedjo Rogers)  
[7f4c6bb](https://gitlab.com/drutopia/drutopia_search/commit/7f4c6bb9b45b6f182ec89029626fcad6f25773ca) Issue #14: Set caching on view to none (Rosemary Mann)  
[fdf4c29](https://gitlab.com/drutopia/drutopia_search/commit/fdf4c29670ba8a03d92ecd2858b18c3bdd5c9fd4) Merge branch '11-search-link' into '8.x-1.x' (Rosemary Mann)  
[f20ada4](https://gitlab.com/drutopia/drutopia_search/commit/f20ada4c0c968f23c02348aff0a0d4be8497209b) Issue #11: add search menu link (Nedjo Rogers)  
[bacf431](https://gitlab.com/drutopia/drutopia_search/commit/bacf431edd96656b722f216cb2c6474be46a7c55) Merge branch '13-facets' into '8.x-1.x' (Nedjo Rogers)  
[8a0c303](https://gitlab.com/drutopia/drutopia_search/commit/8a0c303150772e9f65ba5b40e686f3282c9e22b2) Issue #13: Add facets to search results page (Rosemary Mann)  
[58eeaef](https://gitlab.com/drutopia/drutopia_search/commit/58eeaef1eb2e449a37ab02c9b8afd85f461f281f) Merge branch '10-views-row' into '8.x-1.x' (Nedjo Rogers)  
[5ba3799](https://gitlab.com/drutopia/drutopia_search/commit/5ba3799d32aa53b7da64bf6fc443900641452b09) Issue #10: uncheck add views row classes (Rosemary Mann)  
### drutopia_seo
[837848d](https://gitlab.com/drutopia/drutopia_seo/commit/837848d70e6276693e9b8b4523f32452719e86f0) Merge branch '12-redirect' into '8.x-1.x' (Nedjo Rogers)  
[148baed](https://gitlab.com/drutopia/drutopia_seo/commit/148baede4538e8563d7109161c5ad5e0908eba42) Issue #12: Add Redirect module including permissions and update (Rosemary Mann)  
### drutopia_storyline
[f8ebde4](https://gitlab.com/drutopia/drutopia_storyline/commit/f8ebde43a4dba9b19445d3e03132abb427a439da) Update dependencies just for appearances (benjamin melançon)  ### drutopia_user
### octavia
[ea28bba](https://gitlab.com/drutopia/octavia/commit/ea28bbaa44e611e2b098f20bc2912c5253da9919) Issue #57: adjust page title size at smaller breakpoints (Nedjo Rogers)  
[659b3fa](https://gitlab.com/drutopia/octavia/commit/659b3fa96d8f3b93b58366223435e1f8c35af1ba) Merge branch '59-logo-size' into '8.x-1.x' (Rosemary Mann)  
[868a1a9](https://gitlab.com/drutopia/octavia/commit/868a1a9f429d092bf83688e25bd408cb64c25373) Merge branch '64-address-regressions' into '8.x-1.x' (Rosemary Mann)  
[796f498](https://gitlab.com/drutopia/octavia/commit/796f498467059299c4238dfa8bcb14233d6c235f) Issue #59: increase logo size (Nedjo Rogers)  
[6d24a9a](https://gitlab.com/drutopia/octavia/commit/6d24a9a8e0e4fc4db516ac6fd0cf76a7f9302367) Issue #64: Address block theming regressions (Nedjo Rogers)  
[dc1492f](https://gitlab.com/drutopia/octavia/commit/dc1492f59dc72fab56ee8afe9e06bff18c29fc42) Merge branch '65-move-search-block' into '8.x-1.x' (Rosemary Mann)  
[78282e3](https://gitlab.com/drutopia/octavia/commit/78282e3246803ba8072c1338e6c5d7095adb6d03) Issue #65: move search block to content region (Nedjo Rogers)  
[5989452](https://gitlab.com/drutopia/octavia/commit/59894522ffcee83543543ac7648f921dcf7a8a5a) Merge branch '57-header-refresh' into '8.x-1.x' (Rosemary Mann)  
[afcaf03](https://gitlab.com/drutopia/octavia/commit/afcaf031bf5c443c6a10139884b9c4e10b4b0fca) Issue #57: fix add is-1 class to page title (Nedjo Rogers)  
[68e0ba7](https://gitlab.com/drutopia/octavia/commit/68e0ba7bb6039d4f7e0e70344ff57679dddae886) Issue #57: adjust local task menu style (Nedjo Rogers)  
[9c18d87](https://gitlab.com/drutopia/octavia/commit/9c18d87958052ac578bc8f9e3088bbad44ef4f7e) Issue #57: add is-1 class to page title (Nedjo Rogers)  
[e735a63](https://gitlab.com/drutopia/octavia/commit/e735a63b4607addfeadb7e5bce6e2470f70e11a5) Issue #57: remove bg colour from header (Nedjo Rogers)  
[6051890](https://gitlab.com/drutopia/octavia/commit/60518908127129f3151f618baba0bf83e084a492) Merge branch 'account-menu' into '8.x-1.x' (Nedjo Rogers)  
[8413f6f](https://gitlab.com/drutopia/octavia/commit/8413f6f181854e1c9ae27a59af9da4d9886c3ff4) Move user account menu block to new region (Rosemary Mann)  
[b4625d1](https://gitlab.com/drutopia/octavia/commit/b4625d1a4a871de9ff6e592fa2d530d729fdbdad) Merge branch '57-refresh-header' into '8.x-1.x' (Rosemary Mann)  
[6b918f4](https://gitlab.com/drutopia/octavia/commit/6b918f44f71b3e0f6463adbddaa6330917a49b4f) Issue #57: add region and handling for user account menu (Nedjo Rogers)  
[ef21e0f](https://gitlab.com/drutopia/octavia/commit/ef21e0f1fdbbf6e7130e08c40dd83b979b63b114) Merge branch '61-ds-tweaks' into '8.x-1.x' (Nedjo Rogers)  
[cd5e84f](https://gitlab.com/drutopia/octavia/commit/cd5e84fd0532f660938b4870e16d4b2ce34e7641) Issue #61: Updates to ds templates after image work (Rosemary Mann)  
[d68b29b](https://gitlab.com/drutopia/octavia/commit/d68b29b44d952352969ca117d55e9f7fec81e069) Merge branch '58-refresh-footer-region' into '8.x-1.x' (Rosemary Mann)  
[fd94245](https://gitlab.com/drutopia/octavia/commit/fd942458ccbcf31e2a092f0bedbd1c14c14734e2) Issue #58: refresh footer region (Nedjo Rogers)  
[097df5f](https://gitlab.com/drutopia/octavia/commit/097df5f77d4ed02bf4d70b721621af8e4b6ee786) Merge branch '11-search-link' into '8.x-1.x' (Rosemary Mann)  
[9240594](https://gitlab.com/drutopia/octavia/commit/92405946430b84c01d3838fa3c820562a6075d55) Issue #11: convert the search menu item title to an icon (Nedjo Rogers)  
[47f7df1](https://gitlab.com/drutopia/octavia/commit/47f7df157b158a47a9c226490537015a1c3343a4) Merge branch '56-columns-spacing' into '8.x-1.x' (Rosemary Mann)  
[c1b01be](https://gitlab.com/drutopia/octavia/commit/c1b01be4b2b05a85ddea4147eefd86938d175ff2) Issue #56: fix spacing issue on views using columns within view modes (Nedjo Rogers)  
[170654a](https://gitlab.com/drutopia/octavia/commit/170654aa3477c70b50d52a3ad3d9621cd34573dc) Merge branch '55-views-columns' into '8.x-1.x' (Rosemary Mann)  
[1bd7acb](https://gitlab.com/drutopia/octavia/commit/1bd7acbab4b66fad8cb6cb1f8e8d19d2c7fcd9fd) Issue #55: fix views elements such as pager and RSS feeds displayed don't clear (Nedjo Rogers)  

## 1.0-alpha7

### drutopia_action
### drutopia_article
### drutopia_blog
### drutopia_campaign
[12261ea](https://gitlab.com/drutopia/drutopia_campaign/commit/12261eabda8cb344a3080630f81a37c91255f5d6) Update field_group to beta release (Nedjo Rogers)  
[91541a1](https://gitlab.com/drutopia/drutopia_campaign/commit/91541a16fecf5170cfc0493d319d0af9c67b8b3a) Merge branch '7-tweak-view' into '8.x-1.x' (Nedjo Rogers)  
[69611fe](https://gitlab.com/drutopia/drutopia_campaign/commit/69611fe732799bbc6c9248b5293c0be9f39b40f7) Issue #7: Tweak view to unset row class (Rosemary Mann)  
[e9ca1e1](https://gitlab.com/drutopia/drutopia_campaign/commit/e9ca1e1544829760c39b825a3de0ad404eb3b808) Merge branch '7-refactor-view' into '8.x-1.x' (Nedjo Rogers)  
[f3ebc28](https://gitlab.com/drutopia/drutopia_campaign/commit/f3ebc280c5a2b4572f3ba1d5669af245e4698d4e) Issue #7: refactor to use view based on search index (Rosemary Mann)  
[f68b724](https://gitlab.com/drutopia/drutopia_campaign/commit/f68b7249f1374fa71e2395d4568486aceba53009) Merge branch 'campaign-simple-card' into '8.x-1.x' (Nedjo Rogers)  
[3386bda](https://gitlab.com/drutopia/drutopia_campaign/commit/3386bdaa9d3bc3e06f097436db54741c4799e8f5) Add and configure simple card for campaign (Rosemary Mann)  
[8ddd14a](https://gitlab.com/drutopia/drutopia_campaign/commit/8ddd14ae3cb0af5bd05c9c6f97db3df77873ab40) Merge branch 'cleanup-campaign' into '8.x-1.x' (Nedjo Rogers)  
[3edfd3d](https://gitlab.com/drutopia/drutopia_campaign/commit/3edfd3d8476425bd2d84fd58ebb4c7ff38b9c5f4) Clean up campaign feature (Rosemary Mann)  
### drutopia_comment
### drutopia_core
[1fbd3a4](https://gitlab.com/drutopia/drutopia_core/commit/1fbd3a46a9a85051a23838110ffa05d43a60c740) Merge branch '29-view-modes' into '8.x-1.x' (Nedjo Rogers)  
[f6ba05b](https://gitlab.com/drutopia/drutopia_core/commit/f6ba05b2525fcc3ee5ae8e23f7f5a9d402162860) Issue #29: Add new view modes (Rosemary Mann)  
### drutopia_custom
### drutopia_event
### drutopia_fundraising
### drutopia
[58418bf](https://gitlab.com/drutopia/drutopia/commit/58418bfdc569de8aa309ecb61a084a466d883ec2) Everything is a checklist item.  Everything. (benjamin melançon)  
[bcefa42](https://gitlab.com/drutopia/drutopia/commit/bcefa427bd00502b13fabb30034b0022891424fc) Add instructions; link to docs; add headings; simpler language (benjamin melançon)  
[3f678d1](https://gitlab.com/drutopia/drutopia/commit/3f678d12a75a402375c6ddfc949b217ecfd5c9ed) Add issue template with checklist for new release (Rosemary Mann)  
[3d592a3](https://gitlab.com/drutopia/drutopia/commit/3d592a35d24ca36ea028e981a42f0584583d99be) Merge branch 'pin-config-sync' into '8.x-1.x' (Nedjo Rogers)  
[7058286](https://gitlab.com/drutopia/drutopia/commit/705828674faa9ae341a3050a49ce8057fdcb1653) Work around config sync growing pains (benjamin melançon)  
### drutopia_group
### drutopia_landing_page
[ed19dcf](https://gitlab.com/drutopia/drutopia_landing_page/commit/ed19dcf8403ae2a3a39adf7c6f79c5287503f3d6) Merge branch '8-hide-promotion' into '8.x-1.x' (mlncn)  
[d5b14c3](https://gitlab.com/drutopia/drutopia_landing_page/commit/d5b14c36e8335c518baecbb85a0f1965c080d532) Hide promotion options (Clayton Dewey)  
### drutopia_page
### drutopia_people
### drutopia_related_content
### drutopia_resource
### drutopia_search
### drutopia_seo
### drutopia_site
[d7cbcb4](https://gitlab.com/drutopia/drutopia_site/commit/d7cbcb4f685f434e0b2b656b7cbd1e9b3a2279f3) Merge branch '10-block-configuration' into '8.x-1.x' (Nedjo Rogers)  
[0490654](https://gitlab.com/drutopia/drutopia_site/commit/04906546efecd935ce6b095e9ecec7aaaaa7969d) Issue #10: Block configuration for slide (Rosemary Mann)  
### drutopia_social
### drutopia_storyline
[02bb10d](https://gitlab.com/drutopia/drutopia_storyline/commit/02bb10d4fad30a9b4ae3193dc3a57dc03f73a3c2) Update field_group to beta release (Nedjo Rogers)  
### drutopia_user
### octavia
[af4a624](https://gitlab.com/drutopia/octavia/commit/af4a6240b29f2440d1376876e5616896c4f30764) Merge branch '50-homepage' into '8.x-1.x' (Nedjo Rogers)  
[8c280f3](https://gitlab.com/drutopia/octavia/commit/8c280f377f5b9ad505ef80b8534b9fec256ce15c) Issue #50: fix spacing issue (Nedjo Rogers)  
[620df5b](https://gitlab.com/drutopia/octavia/commit/620df5b0e67e4674e0caebff21736a8e9fcebdeb) Issue #50: Homepage changes (Rosemary Mann)  
