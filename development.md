# Developing for Drutopia

This document covers writing code for and contributing configuration to Drutopia features and the Drutopia distribution.

Drutopia code can be divided into five groups:

* [Drupal 9 core](https://www.drupal.org/project/drupal).
* Drutopia install profile (drutopia).
* Drutopia features (Drutopia Article, etc.).
* Contributed Drupal modules and themes aiding functionality of these features (pathauto, bulma, etc.).
* Contributed Drupal modules supporting the packaging up and upgrading features:
  * [Features](https://www.drupal.org/project/features)
  * [Configuration Update Manager](https://www.drupal.org/project/config_update)


## Repositories

Drutopia features and install profile are hosted on [GitLab](https://gitlab.com/drutopia) and mirrored on [Drupal.org](https://www.drupal.org).

For introductions on using GitLab, see:
* [GitLab issues documentation](https://docs.gitlab.com/ee/user/project/issues/).
* [GitLab merge requests documentation](https://docs.gitlab.com/ee/user/project/merge_requests/).
* The video (on YouTube) [Getting started with Git and GitLab](https://www.youtube.com/watch?v=7p0hrpNaJ14).


## Issue queues

To post suggestions or bug reports, use the issue queues for the GitLab projects linked above. See the GitLab documentation on [creating an issue](https://docs.gitlab.com/ee/gitlab-basics/create-issue.html).

## Geting set up with Git and GitLab

To get started contributing code-level changes to Drutopia features or the Drutopia distribution, see the [GitLab basics](https://docs.gitlab.com/ee/gitlab-basics/README.html) documentation page for tips.

You will need to:
* [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your computer.
* [Create an account on GitLab](https://gitlab.com/users/sign_in) and [add your SSH key](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).


## Local development environment setup

There are many possible ways to set up a local development environment. These instructions accommodate basic LAMP (Linux, Apache, MySQL/MariaDB, PHP) as well as [DDEV](quickstart-ddev) setups. For more information, see the [drupal.org documentation on setting up a local development environment](https://www.drupal.org/docs/develop/local-server-setup). See also the [drupal.org page on where to look for support](https://www.drupal.org/support).


### Useful utilities

An invaluable utility to help with Drupal site development is [Drush](https://www.drush.org/).


### Development installation with DDEV

```bash
mkdir drutopia-dev-site
cd drutopia-dev-site
ddev config --docroot=web --project-type=drupal10 --webserver-type=apache-fpm --database=mariadb:10.8 --php-version=8.2 --create-docroot
ddev auth ssh
ddev composer create -y drutopia/drutopia_dev_template:dev-main --no-interaction
ddev composer run make-repos-pushable
```

### Development installation without DDEV

#### Download and install Composer

Composer is a dependency manager for PHP. It manages core dependencies for Drupal Core and we use it for the same purpose with Drutopia. 

[Install composer](https://getcomposer.org/download/)

#### Download Drupal and all Drutopia dependencies

Run `composer create-project drutopia/drutopia_dev_template:dev-main --keep-vcs --no-interaction drutopia-dev-site`  
### Install Drutopia through the UI

(Currently a bug with subprofiles means installing through Drush is not recommended.)

* Visit your site and follow the Drupal installation instructions.
* Log into your new site and install the modules needed for development. These include:
 * All Drutopia feature modules (which are listed on the modules page under the heading "Drutopia").
 * Features UI and its dependencies (Features and Configuration Manager Base).
* See [the README](https://gitlab.com/drutopia/drutopia_dev_template#readme) for more.  One hint: `git pull` to get the latest version of the composer template we use for development!

## Work on an issue

We use the [Features](https://www.drupal.org/project/features) module for managing changes to configuration in Drutopia features and the distribution. See the [documentation on Features for Drupal 8](https://www.drupal.org/docs/8/modules/features) for background.

Improvements or fixes should have an accompanying issue in one of the GitLab project issue queues.

* Before you begin, ensure you have committed any changes made for a previous issue, since you'll be reverting your site and otherwise may lose those changes.
* If no GitLab issue exists, create one. If one does exist, make sure it's assigned to you.
* For each project in your local dev site, first make sure you're working with the latest code.
 * The install profile lives at `profiles/contrib/drutopia` and each of the feature modules at `modules/contrib/`:
 * Check out the development branch.
 ```
 git checkout 8.x-1.x
 ```
 * Pull in any changes from the origin:
 ```
 git pull --rebase origin 8.x-1.x
 ```
* Ensure your site reflects the latest code base. You may do this by reinstalling, or by using Drush to restore the site features to their default state. __Warning: you will lose any uncommitted changes you've made.__ From the site's directory, type:
 * `drush features-import-all` or, if you are sure you don't want to review the changes you're losing, `drush features-import-all -y`
* For each project you're going to be working on, create a new branch to work on the issue. A branch like this is often called a _feature branch_. Here is an example for an issue on the Drutopia Site project to enhance the default admin toolbar, where 10 is the issue number.
 ```
 git checkout -b 10-admin_toolbar
 ```


### Make and export the changes

For each feature you're working on, make sure you're working with the latest code.

* `git pull --rebase origin 8.x-1.x`

On the local development site you installed, make the changes required to resolve the issue. This might involve downloading and installing new modules and/or making configuration changes.

Now navigate to the Features admin page and regenerate the feature or features you've worked on. See the [Features module documentation on generating features](https://www.drupal.org/docs/8/modules/features/generate-features). Regenerate the feature. You will need to delete the previously exported version of the feature and replace that with the new export--making sure you don't delete the Git information in a `.git` directory.


### Creating a new feature

We encourage proactive contributions to Drutopia, though it could be helpful to check in with the team about your feature idea.

We determine if a feature is included in Drutopia Base, or in a more specific distribution based on whether the majority of Drutopia user would benefit from the feature. The final call on this can be made by any technical lead.

If you are creating a new feature:

* Create a new project for the feature on GitLab in the drutopia group.
* Clone the project locally.
* On the site where you've done your development, navigate to the Features admin page and generate the feature to package up the configuration you've worked on. Start by editing the configuration for the features bundle assignment plugins.
* Generate the feature and copy its files to the directory where you cloned the repository. For example, if the repository is in a directory called `drutopia_example`, you will end up with a file `drutopia_example/drutopia_example.info.yml`.
* Create a `composer.json` file for the feature. If your feature depends on other Drupal modules, add those dependencies to the composer file's `require` section. You can use an existing Drutopia feature's composer file as a model.
* Add and commit the files, making sure you're creating a new `8.x-1.x` branch before committing:
  * $ `cd /path/to/drutopia_example`
  * $ `git checkout -b 8.x-1.x`
  * $ `git add .`
  * $ `git commit -m "Initial commit of Drutopia Example feature.`
  * $ `git push origin 8.x-1.x`


#### Registering your new feature

1. Create a new project on drupal.org:
    * assign the user 'drutopia' (optionally mlncn, MegaKeegMan, freescholar, wolcen, nedjo, rosemarymann, and others involved) full rights to the project
    * push the 1.x branch
    * create a dev release for the 1.x release
    * edit the project to set 1.x as the default branch
    * administer releases to set 1 as the recommended major version
4. Edit the [`composer.json` file in the Drutopia install profile](https://gitlab.com/drutopia/drutopia/blob/2.0.x/composer.json) to include the new feature.
5. Edit the [`composer.json` file in Drutopia VM](https://gitlab.com/drutopia/drutopia_vm/blob/master/composer.json) to include the new feature.
6. Edit the file [`drutopia.subprofiles.yml`](https://gitlab.com/drutopia/drutopia/blob/2.0.x/drutopia.subprofiles.yml) in the Drutopia project to add the new feature to the subprofiles.


### Add or update code dependencies

If you added any new code dependencies like modules or themes to the feature you're working on, or if you need to update to a new version of those dependencies or of Drupal core, you will need to add these changes to `composer.json` files. These files are used by Drutopia developers.


#### Updating Drupal core

To update to a new release of Drupal core:

* Edit the value for `drutopia-core` in the `composer.json` file in the [Drutopia repository](https://gitlab.com/drutopia/drutopia).


#### Adding or updating contributed modules and themes

First, determine if the module or theme you're adding or updating is specific to a given Drutopia module (for example, adding the Admin Toolbar module to the Drutopia Site module). If so:

* Edit the module's `composer.json` file, adding a section for a new module or theme or changing the version number for an existing one.


### Commit your changes

Once you have completed the changes, you can commit them to your local code base. Note that you may need to follow these steps in more than one project. For example, you may have added a field storage to the Drutopia Core module and added the corresponding field to a second Drutopia module.

* Review what changes have been made. This command will list off changed, added, or deleted files.
 ```
 $ git status
 ```
* Add the changed, added, or deleted files.
 ```
 $ git add -A
 ```
* Commit your changes and push them to GitLab. In this example, `10-admin_toolbar` is the name of the feature branch we're working in.
 ```
 $ git commit -m "Issue #10: add Admin toolbar."
 $ git push origin 10-admin_toolbar
 ```


### Submit a merge request

Back in the GitLab project you're working on, use the website interface to submit what's called a "merge request". See the [documentation on submitting merge requests.](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

In your merge request, reference the issue your work is addressing.


## Tips

Before starting more development, get up-to-date with::

```
composer update
```

