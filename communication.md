# Getting Plugged In

We encourage members, supporters, and contributors to join the conversation through video calls and Zulip chat conversations.  Always keep in mind the [code of conduct](drutopia-code-of-conduct).

## General meetings

A great way to get involved with Drutopia is to join [Agaric's Show & Tell every Thursday at 3pm Eastern](https://agaric.coop/show).

Newcomers and anyone who feels they need to catch up with what's going on a bit are invited to join.

## Chat

Agaric hosts a #Drutopia stream on the collective's Zulip chat instance.  [Ask Agaric](https://agaric.coop/ask) for an invite.

There is also a #Drutopia channel on the [Drupal Slack](https://www.drupal.org/community/contributor-guide/reference-information/talk/tools/slack).

## Issue queues

The many projects that make up the Drutopia initiative are in the process of moving from having their primary homes in our [GitLab.com group](http://gitlab.com/drutopia/) to being entirely handled on Drupal.org— see [drupal.org/project/drutopia](https://www.drupal.org/project/drutopia) for a listing of the feature modules that make up Drutopia.

Post questions or feedback or requests to the queue of the relevant project; if you're not sure you can always use the main Drutopia distribution issue queue: [https://www.drupal.org/project/issues/drutopia](https://www.drupal.org/project/issues/drutopia)

Try searching the issues first— the legacy Gitlab group ones at [gitlab.com/groups/drutopia/issues](https://gitlab.com/groups/drutopia/issues) or the full Drupal issue queue at [drupal.org/project/issues](https://www.drupal.org/project/issues)

## Contact form

[drutopia.org/contact-drutopia](https://drutopia.org/contact-drutopia)

## E-mail

If you have any questions or trouble getting set up contact us at info@drutopia.org and we'd be happy to help.
