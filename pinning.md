# Pinning

*Optional / Experimental* – Hold composer dependencies to Drutopia official tested versions:

Presuming DDEV use:

```bash
ddev ssh
cd /var/www/html/
./vendor/bin/composer-pin-apply web/profiles/contrib/drutopia/bin/drutopia-tested-global-pinnings.sh
```

```{warning}
Currently there is a bug in that script; take the output (`composer require` and a list of fifty things) without the stray colon (` : `) at the end and put it into your terminal.
```

