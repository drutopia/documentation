Purpose and Goals
=================

Mission
-------

Drutopia combines the principles of software freedom, community ownership and intersectional politics to co-develop technologies that meet our needs and reflect our values.

Points of Unity
---------------

As participants in the Drutopia initiative we agree to:

* Be inclusive regarding gender, gender identity, sexual orientation, ethnicity, ability, age, religion, geography, and class.
* Commit to protection of personal information and privacy and freedom from surveillance.
* Value collaboration and cooperation above competition.
* Place human needs over private profit.
* Foster non-hierarchical structures and collective decision-making.


Goals
-----

#. Release a Drupal distribution that meets crucial needs for networked grassroots organizations.
#. Launch fully functional drutopia.org
#. Establish a worker co-op product team
#. Build a sustainable and healthy number of members


drutopia.org goals
------------------

#. Use compelling content to convince grassroots activists to become members.
#. Provide technical information to plug open source community members into the project.
#. Gather donations by creating content that inspires, addresses critiques and demonstrates impact.
