***
THIS IS AN UNAPPROVED DRAFT ONLY FOR DISCUSSION.

Discussion is taking place at https://gitlab.com/drutopia/drutopia-organization/issues/43
***

Mission
=======

Drutopia combines the principles of software freedom, community ownership and intersectional politics to co-develop technologies that meet our needs and reflect our values.



Points of Unity
===============

As participants in the Drutopia initiative we agree to:

* Be inclusive regarding gender, gender identity, sexual orientation, ethnicity, ability, age, religion, geography, and class.
* Commit to protection of personal information and privacy and freedom from surveillance.
* Value collaboration and cooperation above competition.
* Place human needs over private profit.
* Foster non-hierarchical structures and collective decision-making.

Drutopia Software Cooperative
=============================

The Drutopia project is governed cooperatively, with a leadership team providing strategic guidance, technical leads deciding technical decisions and members driving forward the roadmap.

Membership
----------

Membership is open to all, simply -

* Pay an annual membership due of $10-100 ($50 recommended) or one time lifetime membership of $100
* Agree to the [Drutopia Code of Conduct](drutopia-code-of-conduct) and [Points of Unity](goals#points-of-unity).

To apply, visit [drutopia.org/apply](https://drutopia.org/apply).

Based on policies previously agreed through proposals by the software cooperative, the application is approved or denied by the leadership team.

Members are encouraged to request and vote up/down features, report bugs and contribute to the project.

Leadership Team
---------------

The Leadership Team makes strategic decisions regarding Drutopia. This includes vision, mission, goals, outreach strategy and target audiences, and licensing. Decisions are made by consensus.

Joining the Leadership Team

The current leadership team is:

* Ben
* Clayton
* Nedjo
* Rosemary

We are currently looking for a representative for front-end development. Open positions will be filled by consensus from the current leadership team.

Once the project has been established for a longer time a democratic process driven by Drutopia members at large will be implemented.

-------------------------
Leadership Team Elections
-------------------------

Elections of the Leadership Team will happen annually, with our first election to be held November 2020. Each member gets one vote.

Technical Leads
---------------

Technical leads make the final call on technical decisions.

Current technical leads are:

* <a href="https://gitlab.com/mlncn">Ben</a>
* <a href="https://gitlab.com/cedewey">Clayton</a>
* <a href =https://gitlab.com/nedjo">Nedjo</a>
* <a href="https://gitlab.com/rosemarymann">Rosemary</a>


Any member is welcome to request becoming a technical lead by

* asking a current tech lead to become one
* a current tech lead then nominates them
* One other tech lead must approve the person
* Once approved, that person's gitlab account is elevated to the Owner role



Proposals
---------

Members may operate on their own initiative based on pre-established policy (areas of activity, scope of working groups, points of unity, code of conduct, technical policy, etc.), but any new issue that affects other members should be brought before the appropriate working group, and any policy changes must be brought before the full group before implementation.

Working groups are encouraged to make proposals among themselves to determine consensus and operate within their scope of responsibility, but only proposals passed by the full group or the leadership team may be considered binding for Drutopia. Any member may make a proposal to the full group, though it is encouraged to first discuss matters within the appropriate working group.

The criteria for passing a proposal (within a working group, or in the full group) is as follows:

* At least 6 days allowed for members to weigh in on the proposal
  * A shorter period is permitted for proposals labeled URGENT in the Title, along with a specified and valid justification in the Details
  * Agreement by at least 50% of voting members
  * No blocks, unless 90% agreement is reached

Every quarter, all non-paying members receive an email checking in about their status that contains the per-user cost of the previous quarter’s operation. The email asks them to set up contributions if their financial situation has changed, or to renew their membership for the next three months, until the next quarterly email.

The leadership team may freeze the account of a member in the case of:

* Violations of the code of conduct or other policies, after at least two warnings
* 6 months of unpaid dues

Any appeals of application denials or frozen accounts will be taken up by the leadership team.

Expenses
--------

Drutopia is run through a system of open contributions. There are no employees, but the cooperative strives for fair and sustainable remuneration for its contributors.

The procedure for paying contributors and expenses is as follows:

* A member makes a proposal to the leadership team (with the Title labeled EXPENSE), detailing either:
* work to be done, the expected timeline, and the contributor's qualifications, and the cost (in USD)
* a service to be acquired, details about the provider, its benefits over alternatives, and the cost
* a donation to be made to an external open-source developer or collective, the work they do that benefits Drutopia members, and a proposed amount
* If that proposal passes, the member may proceed. They may post an invoice to OpenCollective, with a link back to the relevant GitLab issue, for up to one half of the total amount.
* When the work is complete (or equivalent), the member must post a follow-up proposal (with the Title labeled INVOICE, in the same thread as the EXPENSE), including details demonstrating the work done or service obtained
* The contributor should post the expense to the OpenCollective page with a link back to the relevant GitLab issue
* Once passed, the expense is approved by an OpenCollective admin, and the contributor is paid within two weeks, or as soon as the OpenCollective account has sufficient balance

While general budgeting decisions are expected to be passed in the full group, all members are invited to monitor the leadership team, and to sit in on calls if they wish to participate in maintaining accountability.

Diversity
---------

Drutopia is a global community that values a high degree of diversity among its users, including but not limited to gender/gender identity, nationality, religious background, ethnic and racial identity, sexual orientation, economic status, and disability status. Drutopia strives to reflect this value in its user community through its [Code of Conduct](drutopia-code-of-conduct).

Modifications
-------------

These bylaws may be modified at any time through a proposal passing in the full group or leadership team as follows:

* At least 10 days allowed for members to weigh in
* Agreement by at least 75% of voting members
* No blocks, unless 95% agreement is reached
